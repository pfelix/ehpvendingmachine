﻿using System;
using ehpvendingmachine.Helpers;

namespace ehpvendingmachine.ViewModels
{
    using System.Collections.Generic;
    using Services;
    using Models;
    using System.Windows.Input;
    using Views;
    using GalaSoft.MvvmLight.Command;
    using Xamarin.Forms;

    public class MainViewModel: BaseViewModel
    {
        #region Attributes

        public DataServices _dataServices;
        public ApiServices _apiService;
        public bool _upDate;
        public LocalDb _localDb;

        #endregion

        #region properties

        public TokenResponse Token { get; set; }

        public List<ProductsMachine> ProductsMachineList { get; set; }

        public List<UserMachines> UserMachinesesList { get; set; }

        public List<Change> Changes { get; set; }

        public bool Sync { get; set; }

        public bool UpdateError { get; set; }

        #endregion

        #region ViewModel

        public LoginViewModel Login { get; set; }

        public MachinesViewModel Machines { get; set; }

        public ProductsMachineViewModel ProductsMachine { get; set; }

        public ProductViewModel Product { get; set; }

        public SelectedProductViewModel SelectedProduct { get; set; }

        #endregion

        #region Contructors

        public MainViewModel()
        {
            //digo que ela e igual a ela propria
            _instance = this;

            //faco a ligacao a cada pagina pelo mainviewmodel
            this.Login = new LoginViewModel();
            this._apiService = new ApiServices();
            this._dataServices = new DataServices();
            this._localDb = new LocalDb();

        }

        #endregion


        #region singleton
        //design pattern singleton

        //instancio o proprio objecto, estatico para ser visto em qq lado
        private static MainViewModel _instance;

        //crio um metodo estatico, onde ele se envia a ele proprio
        public static MainViewModel GetInstance()
        {
            //se a instancia nao existe crio-a
            if (_instance == null)
            {
                return new MainViewModel();
            }

            //senao retorno-a
            return _instance;
        }

        #endregion

        //fazer o logout na toolbar.item
        public ICommand LogoutCommand
        {
            // é necessário o Nugt MvvmLightLibs
            // esta livraria deteta o comando e manda para o metodo
            get { return new RelayCommand(Logout); }
        }

        private async void Logout()
        {
            #region Verificar ligação à internet

            var connection = await this._apiService.CheckConnection();

            if (connection.IsSucess)
            {
                _localDb.UpdateDateClienteServer();
            }

            #endregion

            List<UserLocal> userLocalList = _dataServices.Get<UserLocal>(false);

            UserLocal user = new UserLocal
            {
                UserId = userLocalList[0].UserId,
                Password = null,
                UserName = userLocalList[0].UserName,
                Sync = userLocalList[0].Sync
            };

            _dataServices.InsertOrUpdate(user);

            await Application.Current.MainPage.Navigation.PushAsync(new LoginPage());
        }

        public ICommand AboutCommand
        {
            // é necessário o Nugt MvvmLightLibs
            // esta livraria deteta o comando e manda para o metodo
            get { return new RelayCommand(About); }
        }

        private async void About()
        {
            await Application.Current.MainPage.Navigation.PushAsync(new AboutPage());
        }
    }
}

