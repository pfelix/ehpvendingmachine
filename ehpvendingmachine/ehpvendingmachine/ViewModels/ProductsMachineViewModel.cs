﻿using System;

namespace ehpvendingmachine.ViewModels
{
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Windows.Input;
    using GalaSoft.MvvmLight.Command;
    using Xamarin.Forms;
    using Models;
    using Views;

    public class ProductsMachineViewModel : BaseViewModel
    {
        #region attributes

        private ObservableCollection<ProductsMachine> _products;

        private ObservableCollection<Change> _change;

        private ObservableCollection<SelectedProductViewModel> _productsMachine;

        private bool _isRefreshing;
        #endregion


        #region properties

        public ObservableCollection<ProductsMachine> Products
        {
            get { return _products; }

            set { SetValue(ref this._products, value); }
        }

        public ObservableCollection<Change> Changes
        {
            get { return _change; }

            set { SetValue(ref this._change, value); }
        }

        public UserMachines Machine { get; set; }

        public ObservableCollection<SelectedProductViewModel> ProductsMachineList
        {
            get { return this._productsMachine; }

            set
            {
                SetValue(ref this._productsMachine, value);
            }
        }

        #endregion





        /// <summary>
        /// passa informacao no click
        /// </summary>
        public ICommand RefreshCommand
        {
            get { return new RelayCommand(LoadProductsMachine); }
        }


        /// <summary>
        /// refresh da apgina
        /// </summary>
        public bool IsRefreshing
        {
            get { return this._isRefreshing; }

            set
            {
                SetValue(ref this._isRefreshing, value);
            }
        }

        /// <summary>
        /// construtor
        /// </summary>
        /// <param name="machine"></param>
        public ProductsMachineViewModel(UserMachines machine)
        {
            this.Machine = machine;

            this.Changes = new ObservableCollection<Change>(this.Machine.Changes);

            this.Products = new ObservableCollection<ProductsMachine>(this.Machine.ProductsMachines);


            this.LoadProductsMachine();
        }

        /// <summary>
        /// passa o produto escolhido
        /// </summary>
        /// <returns></returns>
        private IEnumerable<SelectedProductViewModel> ToSelectedProductModels()
        {
            return MainViewModel.GetInstance().ProductsMachineList.Select(c => new SelectedProductViewModel
            {
                ProductsMachineId = c.ProductsMachineId,
                MachineId = c.MachineId,
                ProductId = c.ProductId,
                ProductName = c.ProductName,
                ValidityDate = c.ValidityDate,
                ProductQuantity = c.ProductQuantity,
                Price = c.Price,
                Image = Path(c.Image)

            });
        }

        //caminho da imagem caso exista alguma
        string Path(string path)
        {
            string PathImage;

            if (string.IsNullOrEmpty(path))
            {
                PathImage = "healthy";
            }
            else
            {
                PathImage = $"http://ehpvendingmachine.azurewebsites.net{path.Substring(1)}";
            }

            return PathImage;
        }


        /// <summary>
        /// carrega os produtos da maquina
        /// </summary>
        private void LoadProductsMachine()
        {
            IsRefreshing = true;

            //carrega a lista de produtos da maquina
            MainViewModel.GetInstance().ProductsMachineList = this.Machine.ProductsMachines;

            //carrega a lista de trocos da maquina
            MainViewModel.GetInstance().Changes = this.Machine.Changes;

            //passa o produto escolhido para a outra pagina
            this.ProductsMachineList = new ObservableCollection<SelectedProductViewModel>(this.ToSelectedProductModels());

            IsRefreshing = false;

        }


    }


}

