﻿using System;

namespace ehpvendingmachine.ViewModels
{
    using GalaSoft.MvvmLight.Command;
    using System.Windows.Input;
    using Models;
    using Views;
    using Xamarin.Forms;
    using ehpvendingmachine.Helpers;

    public class SelectedProductViewModel : ProductsMachine
    {
        public ICommand SelectProductCommand
        {
            get { return new RelayCommand(SelectProduct); }
        }

        private async void SelectProduct()
        {
            MainViewModel.GetInstance().Product = new ProductViewModel(this);

            //caso esteja esgotado ou em fim de prazo
            if (MainViewModel.GetInstance().Product.Products.ProductQuantity == 0 || MainViewModel.GetInstance().Product.Products.ValidityDate <= DateTime.Now)
            {
                await Application.Current.MainPage.DisplayAlert(
                    "Error",
                    "This product is no longer available",
                    "Ok");
                return;
            }

            await Application.Current.MainPage.Navigation.PushAsync(new ProductPage());
        }
    }
}
