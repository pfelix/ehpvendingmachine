﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.IO;
using System.Linq;
using System.Windows.Input;
using Android.Database.Sqlite;
using ehpvendingmachine.Interfaces;
using ehpvendingmachine.Services;
using ehpvendingmachine.Views;
using GalaSoft.MvvmLight.Command;
using Java.Sql;
using SQLite.Net;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ehpvendingmachine.ViewModels
{
    using ehpvendingmachine.Helpers;
    using Models;

    public class ProductViewModel : BaseViewModel
    {
        ApiServices _apiService = new ApiServices();

        DataServices _dataService = new DataServices();

        Historic _historic = new Historic();

        #region Attributes

        private bool _isRunnig;

        private bool _isEnabled;

        private int _quantity;

        private bool _upperIsEnabled;

        private bool _lowerIsEnabled;

        private bool _lowerCoinIsEnabled;

        private bool _upperCoinIsEnabled;

        private double _price;

        private double _coin;

        private double _moneyChange;

        #endregion

        #region Properties

        public ProductsMachine Products { get; set; }

        public List<Change> Changes { get; set; }



        /// <summary>
        /// preco da compra
        /// </summary>
        public double Price
        {
            get { return _price; }
            set { SetValue(ref this._price, value); }
        }

        /// <summary>
        /// moedas inseridas para realizar a compra
        /// </summary>
        public double Coin
        {
            get { return _coin; }
            set { SetValue(ref this._coin, value); }
        }

        /// <summary>
        /// trocos recebidos
        /// </summary>
        public double MoneyChange
        {
            get { return _moneyChange; }
            set { SetValue(ref this._moneyChange, value); }
        }

        /// <summary>
        /// bloqueio de botao para aumnetar quantidade
        /// </summary>
        public bool UpperIsEnabled
        {
            get { return _upperIsEnabled; }
            set { SetValue(ref this._upperIsEnabled, value); }
        }

        /// <summary>
        /// bloqueio de botao para baixar quantidade
        /// </summary>
        public bool LowerIsEnabled
        {
            get { return _lowerIsEnabled; }
            set { SetValue(ref this._lowerIsEnabled, value); }
        }

        /// <summary>
        /// bloqueio de botao para baixar o dinheiro inserido
        /// </summary>
        public bool LowerCoinIsEnabled
        {
            get { return _lowerCoinIsEnabled; }
            set { SetValue(ref this._lowerCoinIsEnabled, value); }
        }

        /// <summary>
        /// bloqueio de botao para aumentar o dinheiro inserido
        /// </summary>
        public bool UpperCoinIsEnabled
        {
            get { return _upperCoinIsEnabled; }
            set { SetValue(ref this._upperCoinIsEnabled, value); }
        }

        /// <summary>
        /// bloqueio de botao de compra
        /// </summary>
        public bool IsEnabled
        {
            get { return _isEnabled; }
            set { SetValue(ref this._isEnabled, value); }
        }

        /// <summary>
        /// loader
        /// </summary>
        public bool IsRunnig
        {
            get { return _isRunnig; }
            set { SetValue(ref this._isRunnig, value); }
        }

        /// <summary>
        /// quantidade do produto  que se quer comprar
        /// </summary>
        public int Quantity
        {
            get { return _quantity; }
            set { SetValue(ref this._quantity, value); }
        }

        /// <summary>
        /// construtor
        /// </summary>
        /// <param name="products"></param>
        public ProductViewModel(ProductsMachine products)
        {
            this.Products = products;

            this.Changes = new List<Change>(MainViewModel.GetInstance().Changes);
            this.Changes = Changes.OrderBy(x => x.CurrencyValue).ToList();

            this.LowerIsEnabled = false;
            this.UpperIsEnabled = true;

            this.LowerCoinIsEnabled = false;
            this.UpperCoinIsEnabled = false;
        }

        #endregion

        #region Commands

        /// <summary>
        /// clique na quantidade de produto para diminuir
        /// </summary>
        public ICommand LowerCommand
        {
            // é necessário o Nugt MvvmLightLibs
            // esta livraria deteta o comando e manda para o metodo
            get { return new RelayCommand(LowerQuantity); }
        }

        /// <summary>
        /// clique na quantidade de produto para aumentar
        /// </summary>
        public ICommand UpperCommand
        {
            // é necessário o Nugt MvvmLightLibs
            // esta livraria deteta o comando e manda para o metodo
            get { return new RelayCommand(UpperQuantity); }
        }

        public ICommand LowerCoinCommand
        {
            // é necessário o Nugt MvvmLightLibs
            // esta livraria deteta o comando e manda para o metodo
            get { return new RelayCommand(LowerCoin); }
        }



        public ICommand UpperCoinCommand
        {
            // é necessário o Nugt MvvmLightLibs
            // esta livraria deteta o comando e manda para o metodo
            get { return new RelayCommand(UpperCoin); }
        }


        /// <summary>
        /// metodo de baixar a quantidade
        /// </summary>
        private void LowerQuantity()
        {
            if (Quantity - 1 <= 0)
            {
                Quantity = 0;
                LowerIsEnabled = false;
                UpperIsEnabled = true;
                Price = Quantity * Products.Price;

                this.LowerCoinIsEnabled = false;
                this.UpperCoinIsEnabled = false;

                this.IsEnabled = false;
            }
            else
            {
                Quantity = Quantity - 1;
                LowerIsEnabled = true;
                UpperIsEnabled = true;
                Price = Quantity * Products.Price;

                this.LowerCoinIsEnabled = true;
                this.UpperCoinIsEnabled = true;

                this.IsEnabled = true;
            }
        }

        /// <summary>
        /// metodo de aumentar a quantidade
        /// </summary>
        private void UpperQuantity()
        {
            if (Quantity + 1 >= Products.ProductQuantity)
            {
                Quantity = Products.ProductQuantity;
                UpperIsEnabled = false;
                LowerIsEnabled = true;
                Price = Quantity * Products.Price;

                this.LowerCoinIsEnabled = true;
                this.UpperCoinIsEnabled = true;

                this.IsEnabled = true;
            }
            else
            {
                Quantity = Quantity + 1;
                UpperIsEnabled = true;
                LowerIsEnabled = true;
                Price = Quantity * Products.Price;

                this.LowerCoinIsEnabled = true;
                this.UpperCoinIsEnabled = true;

                this.IsEnabled = true;
            }

        }


        /// <summary>
        /// metodo de baixar as moedas escolhidas
        /// </summary>
        private void LowerCoin()
        {
            Changes = Changes.OrderBy(x => x.CurrencyValue).ToList();

            //o valor do incremento das moedas e igual a menor moeda inserida na maquina
            if (Coin - Changes[0].CurrencyValue <= 0)
            {
                Coin = 0;
                LowerCoinIsEnabled = false;
                UpperCoinIsEnabled = true;
            }
            else
            {
                //o valor do incremento das moedas e igual a menor moeda inserida na maquina
                Coin = Coin - Changes[0].CurrencyValue;
                LowerCoinIsEnabled = true;
                UpperCoinIsEnabled = true;
            }
        }

        /// <summary>
        /// metodo de aumentar as moedas escolhidas
        /// </summary>
        private void UpperCoin()
        {
            Changes = Changes.OrderBy(x => x.CurrencyValue).ToList();

            //o valor do incremento das moedas e igual a menor moeda inserida na maquina
            Coin = Coin + Changes[0].CurrencyValue;
            UpperCoinIsEnabled = true;
            LowerCoinIsEnabled = true;
        }

        /// <summary>
        /// Comando para fazer login normal
        /// </summary>
        public ICommand BuyCommand
        {
            // é necessário o Nugt MvvmLightLibs
            // esta livraria deteta o comando e manda para o metodo
            get { return new RelayCommand(Buy); }
        }

        #endregion

        #region Methods

        //contador de moedas
        public int Count = 0;

        /// <summary>
        /// Metodo para fazer o login através da API
        /// </summary>
        private async void Buy()
        {
            //caso nao escolha uma quantidade
            if (this.Quantity == 0)
            {
                await Application.Current.MainPage.DisplayAlert(
                    "Info",
                    "You must enter a quantity to buy.",
                    "Ok");
                return;
            }

            //caso nao inseia nenhuma moeda
            if (this.Coin == 0)
            {
                await Application.Current.MainPage.DisplayAlert(
                    "Info",
                    "You must enter a coin to buy this product.",
                    "Ok");
                return;
            }

            //caso o valor seja menor que o custo da compra
            if (this.Coin < Price)
            {
                await Application.Current.MainPage.DisplayAlert(
                    "Info",
                    "Did not enter enough money to make this purchase",
                    "Ok");
                return;
            }

            //soma os valores em trocos existente na maquina
            double totalChange = Changes.Sum(x => x.CurrencyValue * x.Quantity);

            //troco a receber em quantidade(2 e a margem de erro)
            double diference = this.Coin - Price;

            //caso a soma dos trocos seja inferior ao dinheiro colocado pelo cliente
            if ((diference + 2) > totalChange)
            {
                await Application.Current.MainPage.DisplayAlert(
                    "Info",
                    "This machine dont have enough changes to give you. Please insert a lower value.",
                    "Ok");
                Coin = 0;
                return;
            }

            //trocos

            //caso a quantidade da moeda seja 0, retira a moeda da equacao
            try
            {
                foreach (var item in Changes)
                {
                    if (item.Quantity == 0)
                    {
                        Changes.Remove(item);
                    }
                }
            }
            catch
            {

            }

            //ordenar do maior ao mais pq
            Changes = Changes.OrderByDescending(x => x.CurrencyValue).ToList();

            //vai buscar apenas o valor e * 100 para ter num inteiro
            var change = Changes.Select(x => x.CurrencyValue * 100).ToList();

            //multiplico por 100 para trabalhar com num inteiro
            Count = Convert.ToInt32(diference * 100);

            //crio um array de 3 colunas[primeira tem valor da moeda, segunda tem quantidade em stock e terceira tem quantidade usada para dar trocos]
            int[,] getCountChange = new int[Changes.Count, 3];

            //mete num array os valores e as quantidades
            for (int j = 0; j < Changes.Count; j++)
            {
                getCountChange[j, 0] = Convert.ToInt32(change[j]);

                getCountChange[j, 1] = Changes[j].Quantity;
            }



            //faz o calculo do troco
            for (int i = 0; i < change.Count; i++)
            {
                //qts moedas sao necessarias para fazer o troco
                getCountChange[i, 2] = Convert.ToInt32(Count / getCountChange[i, 0]);

                //se a quantidade necessaria para fazer trocos for maior ou igual que a qtd disponivel
                if (getCountChange[i, 1] <= getCountChange[i, 2])
                {
                    //contagem e igual ao numero
                    getCountChange[i, 2] = getCountChange[i, 1];

                    //guarda o resto da quantidade * o valor da moeda
                    Count = Convert.ToInt32(Count % (getCountChange[i, 0] * getCountChange[i, 2]));
                }
                else
                {
                    //guarda o resto
                    Count = Convert.ToInt32(Count % getCountChange[i, 0]);
                }

            }

            //caso exista trocos por dar
            if (Count != 0)
            {
                await Application.Current.MainPage.DisplayAlert(
                    "Info",
                    "Dont have enough changes to give you. this sale is cancelled",
                    "Ok");
                return;
            }

            this.IsRunnig = true;
            this.IsEnabled = false;

            //produto que sera actualizado na bd
            ProductsMachine myProduct = new ProductsMachine
            {
                ProductsMachineId = Products.ProductsMachineId,
                ProductId = Products.ProductId,
                ProductQuantity = (Products.ProductQuantity - Quantity),
                Price = Products.Price,
                ProductName = Products.ProductName,
                MachineId = Products.MachineId,
                ValidityDate = Products.ValidityDate,
                Image = PathImage(Products.Image)
            };


            List<Change> newChangeList = new List<Change>();

            //contador para actualizar a contagem das moedas
            for (int i = 0; i < Changes.Count; i++)
            {
                newChangeList.Add(new Change
                {
                    ChangeId = Changes[i].ChangeId,
                    CurrencyValueId = Changes[i].CurrencyValueId,
                    CurrencyValue = Changes[i].CurrencyValue,
                    MachineId = Changes[i].MachineId,
                    QuantityLimit = Changes[i].QuantityLimit,
                    Quantity = (Changes[i].Quantity - getCountChange[i, 2])
                });
            }

            //venda que sera inserida na bd
            Sell mySell = new Sell
            {
                ProductsMachineId = Products.ProductsMachineId,
                Quantity = this.Quantity,
                Date = DateTime.Now
            };

            var config = DependencyService.Get<IConfig>();

            //se estiver tudo ok
            using (SQLiteConnection connection = new SQLiteConnection(config.Platform,
                Path.Combine(config.DirectoryDB, "ehpvendingmachine.db3")))
            {

                connection.BeginTransaction();
                //atualiza o campo da quantidade dos produtos
                try
                {
                    //actualiza a base de dados com a quantidade dos produtos
                    Products.ProductQuantity = myProduct.ProductQuantity;
                    _dataService.InsertOrUpdate(myProduct);

                    //actualiza a base de dados com a quantidade dos produtos
                    foreach (var item in newChangeList)
                    {
                        _dataService.InsertOrUpdate(item);

                        Changes.Find(x => x.ChangeId == item.ChangeId).Quantity = item.Quantity;
                    }

                    //introduz a venda na base de dados
                    _dataService.Insert(mySell);

                    //mostra o troco
                    await Application.Current.MainPage.DisplayAlert(
                        "Change",
                        (this.Coin - Price).ToString(),
                        "Ok");

                    //faco o commit
                    connection.Commit();
                }
                catch
                {
                    //senao volta atras no processo
                    connection.Rollback();

                    this.IsRunnig = false;
                    this.IsEnabled = true;

                    await Application.Current.MainPage.DisplayAlert(
                        "Info",
                        "Could not make the purchase",
                        "Ok");

                    await Application.Current.MainPage.Navigation.PopAsync();

                    return;
                }

                try
                {
                    // Cria alerta se change a 10% ou 0%
                    _historic.CreatAlertChange();

                    // Cria alerta se stock menor que 3
                    _historic.CreatAlertStock();

                }
                catch
                {
                    await Application.Current.MainPage.DisplayAlert(
                        "Error",
                        "Unable to register the historic from this sale",
                        "Ok");

                    this.IsRunnig = false;
                    this.IsEnabled = true;

                    await Application.Current.MainPage.Navigation.PopAsync();

                    return;
                }
            }

            var userLocal = _dataService.Get<UserLocal>(false);

            UserLocal user = new UserLocal
            {
                UserId = userLocal[0].UserId,
                UserName = userLocal[0].UserName,
                Password = userLocal[0].Password,
                Sync = false
            };

            _dataService.InsertOrUpdate(user);

            await Application.Current.MainPage.Navigation.PopAsync();

            this.IsRunnig = false;
            this.IsEnabled = true;
        }

        //caminho da imagem caso exista alguma
        string PathImage(string path)
        {
            string PathImage;

            if (path == "healthy")
            {
                PathImage = string.Empty;
            }
            else
            {
                PathImage = "~" + path.Substring(42);
            }

            return PathImage;
        }

        #endregion
    }
}
