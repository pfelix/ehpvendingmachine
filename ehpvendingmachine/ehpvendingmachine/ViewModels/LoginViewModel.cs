﻿using System;
using System.IO;
using System.Linq;
using ehpvendingmachine.Helpers;

namespace ehpvendingmachine.ViewModels
{
    using System.Collections.Generic;
    using Models;
    using Services;
    using Views;
    using GalaSoft.MvvmLight.Command;
    using System.Windows.Input;
    using Xamarin.Forms;

    public class LoginViewModel : BaseViewModel
    {
        #region Attributes

        private bool _isRunnig;
        private bool _isRemembered;
        private bool _isEnabled;
        private string _email;
        private string _password;
        private ApiServices _apiService;
        private DataServices _dataServices;
        private LocalDb _localDb;

        #endregion

        #region Properties

        public bool IsRunnig
        {
            get { return _isRunnig; }
            set { SetValue(ref this._isRunnig, value); }
        }

        public bool IsRemembered
        {
            get { return _isRemembered; }
            set { SetValue(ref this._isRemembered, value); }
        }

        public bool IsEnabled
        {
            get { return _isEnabled; }
            set { SetValue(ref this._isEnabled, value); }
        }

        public string Email
        {
            get { return _email; }
            set { SetValue(ref this._email, value); }
        }

        public string Password
        {
            get { return _password; }
            set { SetValue(ref this._password, value); }
        }

        #endregion

        #region Commands

        /// <summary>
        /// Comando para fazer login normal
        /// </summary>
        public ICommand LoginCommand
        {
            // é necessário o Nugt MvvmLightLibs
            // esta livraria deteta o comando e manda para o metodo
            get { return new RelayCommand(Login); }
        }
        #endregion

        #region Methods

        /// <summary>
        /// Metodo para fazer o login através da API
        /// </summary>
        private async void Login()
        {
            if (string.IsNullOrEmpty(this.Email))
            {
                await Application.Current.MainPage.DisplayAlert(
                    "Error",
                    "You must enter email, please.",
                    "Ok");
                return;
            }

            if (string.IsNullOrEmpty(this.Password))
            {
                await Application.Current.MainPage.DisplayAlert(
                    "Error",
                    "You must enter password, please.",
                    "Ok");
                return;
            }

            this.IsRunnig = true;
            this.IsEnabled = false;

            #region Verificar ligação à internet

            var connection = await this._apiService.CheckConnection();

            var mainViewModel = MainViewModel.GetInstance();

            // Carregar variável UpdateError com o que está na BD
            var upError = _dataServices.Get<Update>(false);

            if (upError.Count == 0)
            {
                mainViewModel.UpdateError = false;
            }
            else
            {
                mainViewModel.UpdateError = upError[0].UpdateError;
            }



            if (!connection.IsSucess)
            {
                UserLocal userLocal = _dataServices.First<UserLocal>(false);

                // Verifica se existe algum login guardado na BD
                if (userLocal == null)
                {
                    await Application.Current.MainPage.DisplayAlert("Error", connection.Message, "Ok");
                    this.IsRunnig = false;
                    this.IsEnabled = true;
                    return;
                }

                // Verifica se login está correto se existir na BD
                if (this.Email == userLocal.UserName && this.Password == userLocal.Password)
                {
                    List<Machine> machineList1 = _dataServices.Get<Machine>(false);

                    // Verifica se BD tem dados para mostrar
                    if (machineList1.Count == 0)
                    {
                        await Application.Current.MainPage.DisplayAlert("Info", "You need to have internet access to get data...", "Ok");
                        this.IsRunnig = false;
                        this.IsEnabled = true;
                        return;
                    }

                    // Verifica se não existem algum erro de sincornização
                    if (mainViewModel.UpdateError)
                    {
                        await Application.Current.MainPage.DisplayAlert("Info", "There was an error in the last sync. You need to have internet access to get data...", "Ok");
                        this.IsRunnig = false;
                        this.IsEnabled = true;
                        return;
                    }
                }
                else
                {
                    await Application.Current.MainPage.DisplayAlert("Info", "You need to have internet access to get data...", "Ok");
                    this.IsRunnig = false;
                    this.IsEnabled = true;
                    return;
                }
            }
            else
            {
                #region Forçar sempre sync

                // Atualiza os dados no servidor e apaga as tabelas da DB local
                _localDb.UpdateDateClienteServer();

                // Carregar variável UpdateError com o que está na BD
                upError = _dataServices.Get<Update>(false);

                if (upError.Count == 0)
                {
                    mainViewModel.UpdateError = false;
                }
                else
                {
                    mainViewModel.UpdateError = upError[0].UpdateError;
                }

                #endregion

                #region Token

                var token = await this._apiService.GetToken("http://ehpvendingmachineapi.azurewebsites.net/", this.Email,
                    this.Password);

                if (token == null)
                {
                    this.IsRunnig = false;
                    this.IsEnabled = true;
                    await Application.Current.MainPage.DisplayAlert("Error", "Something was wrong, please try later.", "Ok");
                    return;
                }

                // Token vem vazio (Exp. login inválido)
                if (string.IsNullOrEmpty(token.AccessToken))
                {
                    this.IsRunnig = false;
                    this.IsEnabled = true;
                    await Application.Current.MainPage.DisplayAlert("Error", token.ErrorDescription, "Ok");
                    return;
                }

                // Atribuir à propriedade que está no MainViewModel o token para ser acedido no resto da aplicação

                mainViewModel.Token = token;

                try
                {
                    // Verificar se na BD local os dados são deste user
                    var userlocal = _dataServices.Get<UserLocal>(false);

                    if (userlocal.Count != 0)
                    {
                        if (userlocal[0].UserName != token.UserName && userlocal[0].Sync == false)
                        {
                            await Application.Current.MainPage.DisplayAlert("Error", "The last user does not have the data synchronized.Turn off the application and reconnect or log in with the last user and perform data synchronization.", "Ok");

                            return;
                        }

                        if (userlocal[0].UserName != token.UserName)
                        {
                            _localDb.DeleteAllDataLocalDb();
                        }
                    }
                }
                catch
                {
                    await Application.Current.MainPage.DisplayAlert("Error", "Failed to verify user on local DB.", "Ok");
                    return;
                }

                #endregion

                #region Verificar se BD tem dados

                List<Machine> machineList = _dataServices.Get<Machine>(false);

                // Caso não tenha dados ou se existiu algum erro ao sincronizar os dados vai à API buscar dados
                if (machineList.Count == 0 || mainViewModel.UpdateError == true)
                {
                    // Garantir que tabelas estão limpas
                    _localDb.DeleteAllDataLocalDb();

                    #region Load UserMachines API

                    var response = await this._apiService.GetList<UserMachines>("https://ehpvendingmachineapi.azurewebsites.net", "/api", "/Machine?email=" + token.UserName);

                    if (!response.IsSucess)
                    {
                        await Application.Current.MainPage.DisplayAlert(
                            "Error",
                            response.Message,
                            "Ok");

                        this.IsRunnig = false;
                        this.IsEnabled = true;

                        this.Email = string.Empty;
                        this.Password = string.Empty;

                        return;
                    }

                    #endregion

                    #region Carregar dados na BD Local

                    UserLocal user = new UserLocal
                    {
                        UserId = 1,
                        UserName = token.UserName,
                        Sync = false
                    };

                    _dataServices.InsertOrUpdate(user);

                    List<UserMachines> userMachinesList = (List<UserMachines>) response.Result;
                    
                    _localDb.SaveLocalDb(userMachinesList);

                    #endregion
                }

                #endregion
            }

            #endregion

            #region Ler dados BD Local
            
            if(mainViewModel.UpdateError)
            {
                await Application.Current.MainPage.DisplayAlert(
                            "Error",
                            "There was an error in the last sync, you need to sync in the upper right corner",
                            "Ok");
            }
            else
            {
                mainViewModel.UserMachinesesList = _localDb.GetUserMachinesesLocalDb();
            }

            #endregion

            #region Verificar se está marcado opção de remember

            if (IsRemembered)
            {
                UserLocal userLocal = new UserLocal
                {
                    UserId = 1,
                    Password = this.Password,
                    UserName = this.Email,
                    Sync = true
                };

                _dataServices.InsertOrUpdate(userLocal);
            }
            else
            {
                List<UserLocal> userLocalList = _dataServices.Get<UserLocal>(false);

                UserLocal user = new UserLocal
                {
                    UserId = userLocalList[0].UserId,
                    Password = null,
                    UserName = userLocalList[0].UserName,
                    Sync = userLocalList[0].Sync
                };

                _dataServices.InsertOrUpdate(user);
            }

            #endregion

            this.IsRunnig = false;
            this.IsEnabled = true;

            this.Email = string.Empty;
            this.Password = string.Empty;

            // Instanciar o CountriesViesModel
            // Só é possivel porque foi criado um Singleton na MaindViewModel
            mainViewModel.Machines = new MachinesViewModel();

            // Passar para outra página
            await Application.Current.MainPage.Navigation.PushAsync(new MachinesPage());
        }



        #endregion

        #region Constructores

        public LoginViewModel()
        {
            this._apiService = new ApiServices();
            this._dataServices = new DataServices();
            this._localDb = new LocalDb();
            this.IsRemembered = true;
            this._isEnabled = true;

            LoadUserLocal();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Lê da BD local o utilizador
        /// </summary>
        private void LoadUserLocal()
        {
            try
            {
                UserLocal userLocal = _dataServices.First<UserLocal>(false);

                if (userLocal.Password != null)
                {
                    this.Email = userLocal.UserName;
                    this.Password = userLocal.Password;
                }
            }
            catch
            {
                
            }
            
        }

        #endregion






    }
}
