﻿namespace ehpvendingmachine.ViewModels
{
    using Services;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Windows.Input;
    using Models;
    using GalaSoft.MvvmLight.Command;
    using Xamarin.Forms;
    using Views;

    public class MachinesViewModel : BaseViewModel
    {

        private ObservableCollection<SelectedMachineViewModel> _machines;

        private bool _isRefreshing;

        private bool _isEnabled;

        /// <summary>
        /// no click pasa a info
        /// </summary>
        public ICommand RefreshCommand
        {
            get { return new RelayCommand(LoadMachines); }
        }

        /// <summary>
        /// refresh da pagina
        /// </summary>
        public bool IsRefreshing
        {
            get { return this._isRefreshing; }

            set
            {
                SetValue(ref this._isRefreshing, value);
            }
        }

        public bool IsEnabled
        {
            get { return _isEnabled; }
            set { SetValue(ref this._isEnabled, value); }
        }


        public ObservableCollection<SelectedMachineViewModel> MachineList
        {
            get { return this._machines; }

            set
            {
                SetValue(ref this._machines, value);
            }
        }

        public MachinesViewModel()
        {
            this.LoadMachines();

        }

        /// <summary>
        /// carrega numa lista as maquinas
        /// </summary>
        /// <returns></returns>
        private IEnumerable<SelectedMachineViewModel> ToSelectedMachineViewModels()
        {
            return MainViewModel.GetInstance().UserMachinesesList.Select(c => new SelectedMachineViewModel
            {
                MachineId = c.MachineId,
                ProductsMachines = c.ProductsMachines,
                Changes = c.Changes

            });
        }

        /// <summary>
        /// carrega as maquinas do utilizador
        /// </summary>
        private void LoadMachines()
        {
            this.IsEnabled = false;
            IsRefreshing = true;

            this.MachineList = new ObservableCollection<SelectedMachineViewModel>(this.ToSelectedMachineViewModels().OrderBy(x => x.MachineId));

            IsRefreshing = false;
            IsEnabled = true;

        }
    }
}
