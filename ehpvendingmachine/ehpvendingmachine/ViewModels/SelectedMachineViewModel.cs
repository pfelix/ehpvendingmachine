﻿namespace ehpvendingmachine.ViewModels
{
    using System.Windows.Input;
    using Models;
    using Views;
    using GalaSoft.MvvmLight.Command;
    using Xamarin.Forms;
    using ehpvendingmachine.Helpers;

    public class SelectedMachineViewModel : UserMachines
    {
        Historic _historic = new Historic();

        public ICommand SelectMachineCommand
        {
            get { return new RelayCommand(SelectMachine); }
        }

        private async void SelectMachine()
        {
            MainViewModel.GetInstance().ProductsMachine = new ProductsMachineViewModel(this);

            if (_historic.CreatAlertProductValidity())
                await Application.Current.MainPage.DisplayAlert("Creat Alert", "Created validity alert.", "Ok");

            await Application.Current.MainPage.Navigation.PushAsync(new ProductsMachinePage());
        }


    }
}
