﻿using System;
using System.Collections.Generic;
using System.Text;
using SQLite.Net.Attributes;

namespace ehpvendingmachine.Models
{
    public class HistoricAlertLocal: HistoricAlert
    {
        [PrimaryKey, AutoIncrement]
        public int HistoricAlertIdLocal { get; set; }

        public override int GetHashCode()
        {
            return HistoricAlertIdLocal;
        }

    }
}
