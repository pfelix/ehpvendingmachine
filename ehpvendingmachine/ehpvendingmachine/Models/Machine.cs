﻿using System;
using System.Collections.Generic;
using System.Text;
using SQLite.Net.Attributes;

namespace ehpvendingmachine.Models
{
    public class Machine
    {
        [PrimaryKey]
        public int MachineId { get; set; }
    }
}
