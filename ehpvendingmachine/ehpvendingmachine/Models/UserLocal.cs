﻿namespace ehpvendingmachine.Models
{
    using SQLite.Net.Attributes;

    public class UserLocal
    {
        // Chave primária no SQLite
        [PrimaryKey]
        public int UserId { get; set; }

        public string UserName { get; set; }

        public string Password { get; set; }

        public bool Sync { get; set; }

        public override int GetHashCode()
        {
            return UserId;
        }
    }
}
