﻿namespace ehpvendingmachine.Models
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using SQLite.Net.Attributes;

    public class ProductsMachine
    {
        [PrimaryKey]
        public int ProductsMachineId { get; set; }

        public int MachineId { get; set; }

        public int ProductId { get; set; }

        public string ProductName { get; set; }

        public DateTime ValidityDate { get; set; }

        public string Image { get; set; }

        public int ProductQuantity { get; set; }

        public  double Price { get; set; }

        public override int GetHashCode()
        {
            return ProductsMachineId;
        }

    }
}
