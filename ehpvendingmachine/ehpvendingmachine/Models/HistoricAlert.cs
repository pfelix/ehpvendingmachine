﻿using System;
using System.Collections.Generic;
using System.Text;
using SQLite.Net.Attributes;

namespace ehpvendingmachine.Models
{
    public class HistoricAlert
    {
        public int HistoricAlertId { get; set; }

        public int MachineId { get; set; }

        public int SetAlertId { get; set; }

        public DateTime Date { get; set; }

    }
}
