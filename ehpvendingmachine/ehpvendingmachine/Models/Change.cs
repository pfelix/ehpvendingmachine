﻿namespace ehpvendingmachine.Models
{
    using SQLite.Net.Attributes;

    public class Change
    {
        [PrimaryKey]
        public int ChangeId { get; set; }

        public int CurrencyValueId { get; set; }

        public double CurrencyValue { get; set; }

        public int MachineId { get; set; }

        public int Quantity { get; set; }

        public int QuantityLimit { get; set; }

        public override int GetHashCode()
        {
            return ChangeId;
        }
    }
}
