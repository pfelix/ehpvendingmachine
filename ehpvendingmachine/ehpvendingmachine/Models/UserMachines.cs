﻿namespace ehpvendingmachine.Models
{
    using SQLite.Net.Attributes;
    using System;
    using System.Collections.Generic;
    using System.Text;

    public class UserMachines
    {
        [PrimaryKey]
        public int MachineId { get; set; }

        public List<ProductsMachine> ProductsMachines { get; set; }

        public List<Change> Changes { get; set; }

        public List<HistoricAlert> HistoricAlerts { get; set; }
    }
}
