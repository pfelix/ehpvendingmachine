﻿using SQLite.Net.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace ehpvendingmachine.Models
{
    public class Update
    {
        [PrimaryKey]
        public int UpdateId { get; set; }

        public bool UpdateError { get; set; }

        public override int GetHashCode()
        {
            return UpdateId;
        }
    }
}
