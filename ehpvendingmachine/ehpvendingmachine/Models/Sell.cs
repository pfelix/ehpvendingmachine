﻿using System;
using System.Collections.Generic;
using System.Text;
using SQLite.Net.Attributes;

namespace ehpvendingmachine.Models
{
    public class Sell
    {
        [PrimaryKey, AutoIncrement]
        public int SellId { get; set; }

        public int ProductsMachineId { get; set; }

        public int Quantity { get; set; }

        public DateTime Date { get; set; }
    }
}
