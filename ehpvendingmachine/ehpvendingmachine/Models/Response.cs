﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace ehpvendingmachine.Models
{
    public class Response
    {
        public bool IsSucess { get; set; }

        public string Message { get; set; }

        public object Result { get; set; }
        
        public HttpStatusCode Code { get; set; }
    }
}
