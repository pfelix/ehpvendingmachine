﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ehpvendingmachine.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class MachinesPage : ContentPage
	{
		public MachinesPage ()
		{
			InitializeComponent ();
		}

	    protected override bool OnBackButtonPressed()
	    {
	        return true;
	    }
	}
}