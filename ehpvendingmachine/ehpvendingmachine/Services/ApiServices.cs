﻿using Xamarin.Forms.Internals;

namespace ehpvendingmachine.Services
{
    using Models;
    using Newtonsoft.Json;
    using Plugin.Connectivity;
    using System;
    using System.Collections.Generic;
    using System.Net.Http;
    using System.Text;
    using System.Threading.Tasks;

    public class ApiServices
    {
        /// <summary>
        /// verifica a conexao a internet
        /// </summary>
        /// <returns></returns>
        public async Task<Response> CheckConnection()
        {
            //detecta se nao ha internet
            if (!CrossConnectivity.Current.IsConnected)
            {
                return new Response
                {
                    IsSucess = false,
                    Message = "Please see your interner config."
                };
            }

            //deteta se ha conexao e nao consegue ir a internet
            var isReachable = await CrossConnectivity.Current.IsRemoteReachable("google.com");

            if (!isReachable)
            {
                return new Response
                {
                    IsSucess = false,
                    Message = "Please see your interner connection."
                };
            }

            //se der tudo responde positivo
            return new Response
            {
                IsSucess = true,
                Message = "Ok"
            };
        }

        /// <summary>
        /// passa os dados da url, nome e password e recebe o token
        /// </summary>
        /// <param name="urlBase"></param>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public async Task<TokenResponse> GetToken(string urlBase, string username, string password)
        {
            try
            {
                var client = new HttpClient();

                client.BaseAddress = new Uri(urlBase);
                //chama o token, dizendo o grant_type, user e pass e o encoding
                var response = await client.PostAsync("Token", new StringContent(
                    string.Format("grant_type=password&userName={0}&password={1}", username, password),
                    Encoding.UTF8, "application/x-www-form-urlencoded"));

                var resultJson = await response.Content.ReadAsStringAsync();

                var result = JsonConvert.DeserializeObject<TokenResponse>(resultJson);

                return result;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        /// <summary>
        /// recebe a url, o prefixo e o controlador e carrega os dados numa lista
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="urlBase"></param>
        /// <param name="servicePrefix"></param>
        /// <param name="controller"></param>
        /// <returns></returns>
        public async Task<Response> GetList<T>(string urlBase, string servicePrefix, string controller)
        {
            try
            {
                var client = new HttpClient();

                client.BaseAddress = new Uri(urlBase);

                //junta o url com o prefixo
                var url = string.Format("{0}{1}", servicePrefix, controller);

                //envia o url completo
                var response = await client.GetAsync(url);

                //RECEBE O JSON
                var result = await response.Content.ReadAsStringAsync();

                //se nao retornar
                if (!response.IsSuccessStatusCode)
                {
                    return new Response
                    {
                        IsSucess = false,
                        Message = result
                    };
                }
                //recebe o objecto e desserializa
                var list = JsonConvert.DeserializeObject<List<T>>(result);

                return new Response
                {
                    IsSucess = true,
                    Message = "Ok",
                    Result = list
                };
            }
            catch (Exception e)
            {
                return new Response
                {
                    IsSucess = false,
                    Message = e.Message
                };
            }
        }

        /// <summary>
        /// get user pelo email e retorna o id
        /// </summary>
        /// <param name="urlBase"></param>
        /// <param name="servicePrefix"></param>
        /// <param name="controller"></param>
        /// <returns></returns>
        public async Task<Response> GetModel<T>(string urlBase, string servicePrefix, string controller)
        {
            try
            {
                var client = new HttpClient();

                client.BaseAddress = new Uri(urlBase);

                //junta o url com o prefixo
                var url = string.Format("{0}{1}", servicePrefix, controller);

                //envia o url completo
                var response = await client.GetAsync(url);

                //RECEBE O JSON
                var result = await response.Content.ReadAsStringAsync();

                //se nao retornar
                if (!response.IsSuccessStatusCode)
                {
                    return new Response
                    {
                        IsSucess = false,
                        Message = result
                    };
                }
                //recebe o objecto e desserializa
                var list = JsonConvert.DeserializeObject<T>(result);

                return new Response
                {
                    IsSucess = true,
                    Message = "Ok",
                    Result = list
                };
            }
            catch (Exception e)
            {
                return new Response
                {
                    IsSucess = false,
                    Message = e.Message
                };
            }
        }

        /// <summary>
        /// meter na base de dados pela api
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="urlBase"></param>
        /// <param name="servicePrefix"></param>
        /// <param name="controller"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<Response> AddModelAsync<T>(string urlBase, string servicePrefix, string controller, T model)
        {
            try
            {
                var client = new HttpClient();

                client.BaseAddress = new Uri(urlBase);

                var url = string.Format("{0}{1}", servicePrefix, controller);

                var data = JsonConvert.SerializeObject(model);
                var content = new StringContent(data, Encoding.UTF8, "application/json");

                HttpResponseMessage response = null;

                response = await client.PostAsync(url, content);

                if (!response.IsSuccessStatusCode)
                {
                    return new Response
                    {
                        IsSucess = false,
                        Message = response.ReasonPhrase,
                        Code = response.StatusCode
                    };
                }

                return new Response
                {
                    IsSucess = true,
                    Message = data,
                    Code = response.StatusCode
                };

            }
            catch (Exception ex)
            {
                return new Response
                {
                    IsSucess = false,
                    Message = ex.Message
                };
            }
        }

        /// <summary>
        /// atualizar dados na bd pela api
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="urlBase"></param>
        /// <param name="servicePrefix"></param>
        /// <param name="controller"></param>
        /// <param name="id"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<Response> UpdateModelAsync<T>(string urlBase, string servicePrefix, string controller, int id, T model)
        {
            try
            {
                var client = new HttpClient();

                client.BaseAddress = new Uri(urlBase);

                //junta o url com o prefixo
                var url = string.Format("{0}{1}/{2}", servicePrefix, controller, id);

                var data = JsonConvert.SerializeObject(model);

                var content = new StringContent(data, Encoding.UTF8, "application/json");

                HttpResponseMessage response = null;
                response = await client.PutAsync(url, content);

                if (!response.IsSuccessStatusCode)
                {
                    return new Response
                    {
                        IsSucess = false,
                        Message = response.ReasonPhrase,
                        Code = response.StatusCode
                    };
                }

                return new Response
                {
                    IsSucess = true,
                    Message = data,
                    Code = response.StatusCode
                };

            }
            catch (Exception ex)
            {
                return new Response
                {
                    IsSucess = false,
                    Message = ex.Message
                };
            }
        }

    }
}
