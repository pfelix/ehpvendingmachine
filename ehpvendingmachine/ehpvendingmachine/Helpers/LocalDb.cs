﻿using System.Net;

namespace ehpvendingmachine.Helpers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using ehpvendingmachine.Models;
    using ehpvendingmachine.Services;
    using ehpvendingmachine.ViewModels;
    using Xamarin.Forms;

    public class LocalDb
    {
        private DataServices _dataServices;
        private ApiServices _apiServices;

        public LocalDb()
        {
            this._dataServices = new DataServices();
            this._apiServices = new ApiServices();
        }

        /// <summary>
        /// Guarda dados recebidos da API na BD local e guarda da variável UpdateError
        /// </summary>
        /// <param name="userMachinesList"></param>
        public void SaveLocalDb(List<UserMachines> userMachinesList)
        {
            try
            {
                for (int i = 0; i < userMachinesList.Count; i++)
                {
                    Machine machine = new Machine();

                    machine.MachineId = userMachinesList[i].MachineId;

                    _dataServices.Insert(machine);

                    // Guardar dados dos Produtos Machine
                    foreach (var productsMachine in userMachinesList[i].ProductsMachines)
                    {
                        ProductsMachine newProductsMachine = new ProductsMachine
                        {
                            ProductsMachineId = productsMachine.ProductsMachineId,
                            MachineId = productsMachine.MachineId,
                            ProductId = productsMachine.ProductId,
                            ProductName = productsMachine.ProductName,
                            ValidityDate = productsMachine.ValidityDate,
                            Image = productsMachine.Image,
                            ProductQuantity = productsMachine.ProductQuantity,
                            Price = productsMachine.Price
                        };

                        _dataServices.InsertOrUpdate(newProductsMachine);
                    }

                    // Guardar dados dos trocos
                    foreach (var change in userMachinesList[i].Changes)
                    {
                        _dataServices.InsertOrUpdate(change);
                    }

                    // Guardar dados dos Alertas por resolver
                    foreach (var historicAlert in userMachinesList[i].HistoricAlerts)
                    {
                        HistoricAlertLocal historicAlertLocal = new HistoricAlertLocal
                        {
                            HistoricAlertId = historicAlert.HistoricAlertId,
                            MachineId = historicAlert.MachineId,
                            SetAlertId = historicAlert.SetAlertId,
                            Date = historicAlert.Date
                        };

                        _dataServices.InsertOrUpdate(historicAlertLocal);
                    }
                }

                // Guarda na BD local
                InsertUpdateError(false);
            }
            catch
            {
                // Guarda na BD local
                InsertUpdateError(true);
            }
        }

        /// <summary>
        /// Obter Lista do tipo UserMachines da BD
        /// </summary>
        /// <returns></returns>
        public List<UserMachines> GetUserMachinesesLocalDb()
        {
            List<Machine> machineListe = _dataServices.Get<Machine>(false);

            List<UserMachines> userMachines = new List<UserMachines>();

            for (int i = 0; i < machineListe.Count; i++)
            {
                UserMachines userMachine = new UserMachines();

                List<Change> changeList = _dataServices.Get<Change>(false);
                List<ProductsMachine> productsMachineList = _dataServices.Get<ProductsMachine>(false);

                userMachine.MachineId = machineListe[i].MachineId;
                userMachine.Changes = changeList.Where(c => c.MachineId == machineListe[i].MachineId).ToList();
                userMachine.ProductsMachines = productsMachineList.Where(c => c.MachineId == machineListe[i].MachineId).ToList();

                userMachines.Add(userMachine);
            }

            return userMachines;
        }

        /// <summary>
        /// Apagar todos os dados da BD local do tipo UserMachines
        /// </summary>
        public void DeleteAllDataLocalDbUserMachineses()
        {
            _dataServices.DeleteAll<Machine>();
            _dataServices.DeleteAll<Change>();
            _dataServices.DeleteAll<ProductsMachine>();
            _dataServices.DeleteAll<HistoricAlertLocal>();
        }

        /// <summary>
        /// Apagar todos os dados da BD local
        /// </summary>
        public void DeleteAllDataLocalDb()
        {
            _dataServices.DeleteAll<UserLocal>();
            _dataServices.DeleteAll<Machine>();
            _dataServices.DeleteAll<Change>();
            _dataServices.DeleteAll<ProductsMachine>();
            _dataServices.DeleteAll<HistoricAlertLocal>();
            _dataServices.DeleteAll<Sell>();
        }

        /// <summary>
        /// Apagar todos os dados da BD local
        /// </summary>
        public void DeleteAllDataLocalDbExceptUserLocal()
        {
            _dataServices.DeleteAll<Machine>();
            _dataServices.DeleteAll<Change>();
            _dataServices.DeleteAll<ProductsMachine>();
            _dataServices.DeleteAll<HistoricAlertLocal>();
            _dataServices.DeleteAll<Sell>();
        }

        /// <summary>
        /// Metodo que atualiza os dados sentido cliente > servidor
        /// </summary>
        public async void UpdateDateClienteServer()
        {
            try
            {
                UpdateSellsTable();

                UpdateHistoricAlertTable();

                // Verificar se tabela change foi alterda no servidor
                // Se não foi alterada atualiza o servidor
                // Este processo está a ser realizadao diretamente na action da API
                UpdateChangeTable();

                // Verificar se tabela ProductsMachine foi alterda no servidor
                // Se não foi alterada atualiza o servidor
                // Este processo está a ser realizadao diretamente na action da API
                UpdateProductsMachineTable();

                // Apagar dados da tabela Machine
                //_dataServices.DeleteAll<Machine>();

                List<UserLocal> userLocal = _dataServices.Get<UserLocal>(false);

                if (userLocal.Count != 0)
                {
                    UserLocal user = new UserLocal
                    {
                        UserId = userLocal[0].UserId,
                        Password = userLocal[0].Password,
                        UserName = userLocal[0].UserName,
                        Sync = true
                    };

                    _dataServices.InsertOrUpdate(user);
                }
            }
            catch
            {
                // Guarda na BD local
                InsertUpdateError(true);
                await Application.Current.MainPage.DisplayAlert("Error", "There was a problem updating the data from cliente to server.", "Ok");
            }
        }

        /// <summary>
        /// Atualiza os dados no servidor na tabela ProductsMachines
        /// </summary>
        private async void UpdateProductsMachineTable()
        {
            List<ProductsMachine> productsMachineList = new List<ProductsMachine>();

            productsMachineList = _dataServices.Get<ProductsMachine>(false);

            // Inserir dados na tabela ProductsMachine
            try
            {
                foreach (var productsMachine in productsMachineList)
                {
                    ProductsMachine myProductsMachineList = new ProductsMachine
                    {
                        ProductsMachineId = productsMachine.ProductsMachineId,
                        MachineId = productsMachine.MachineId,
                        ProductId = productsMachine.ProductId,
                        ValidityDate = productsMachine.ValidityDate,
                        ProductQuantity = productsMachine.ProductQuantity
                    };

                    var response = await _apiServices.UpdateModelAsync("http://ehpvendingmachineapi.azurewebsites.net/", "api/", "ProductsMachines/", myProductsMachineList.ProductsMachineId,
                        myProductsMachineList);

                    if (response.Code == HttpStatusCode.NotModified)
                    {
                        InsertUpdateError(true);
                    }
                }

                //_dataServices.DeleteAll<ProductsMachine>();
            }
            catch (Exception e)
            {
                InsertUpdateError(true);

                await Application.Current.MainPage.DisplayAlert(
                    "Error Products Machine",
                    e.Message,
                    "Ok");
            }
        }

        /// <summary>
        /// Atualiza os dados no servidor na tabela Changes
        /// </summary>
        private async void UpdateChangeTable()
        {
            List<Change> changeList = new List<Change>();

            changeList = _dataServices.Get<Change>(false);

            // Inserir dados na tabela Change
            try
            {
                foreach (var change in changeList)
                {
                    Change mychange = new Change
                    {
                        ChangeId = change.ChangeId,
                        CurrencyValueId = change.CurrencyValueId,
                        CurrencyValue = change.CurrencyValue,
                        MachineId = change.MachineId,
                        Quantity = change.Quantity,
                        QuantityLimit = change.QuantityLimit
                    };

                   var response = await _apiServices.UpdateModelAsync("http://ehpvendingmachineapi.azurewebsites.net/", "api/", "Changes/",mychange.ChangeId,
                        mychange);

                    if (response.Code == HttpStatusCode.NotModified)
                    {
                        InsertUpdateError(true);
                    }
                }

                //_dataServices.DeleteAll<Change>();
            }
            catch (Exception e)
            {
                InsertUpdateError(true);

                await Application.Current.MainPage.DisplayAlert(
                    "Error Change",
                    e.Message,
                    "Ok");
            }
        }

        /// <summary>
        /// Atualiza os dados no servidor na tabela HistoricAlerts
        /// </summary>
        private async void UpdateHistoricAlertTable()
        {
            List<HistoricAlertLocal> historicAlertList = new List<HistoricAlertLocal>();

            // Apaga os resgistos que ainda não foram passados para o servidor, ou seja sem HistoricAlertId
            historicAlertList = _dataServices.Get<HistoricAlertLocal>(false).Where(h => h.HistoricAlertId == 0).ToList();

            // Inserir dados na tabela HistoricAlert
            try
            {

                foreach (var historicAlert in historicAlertList)
                {
                    HistoricAlert myHistoricAlert = new HistoricAlert
                    {
                        HistoricAlertId = 0,
                        MachineId = historicAlert.MachineId,
                        SetAlertId = historicAlert.SetAlertId,
                        Date = historicAlert.Date
                    };

                    var response = await _apiServices.AddModelAsync("http://ehpvendingmachineapi.azurewebsites.net/", "api/", "HistoricAlerts",
                        myHistoricAlert);
                }

                //_dataServices.DeleteAll<HistoricAlertLocal>();

            }
            catch (Exception e)
            {
                InsertUpdateError(true);

                await Application.Current.MainPage.DisplayAlert(
                    "Error Historic Alert",
                    e.Message,
                    "Ok");
            }
        }

        /// <summary>
        /// Atualiza os dados no servidor na tabela Sells
        /// </summary>
        private async void UpdateSellsTable()
        {
            List<Sell> sellList = new List<Sell>();

            sellList = _dataServices.Get<Sell>(false);

            // Inserir dados na tabela Sells
            try
            {
                foreach (var sell in sellList)
                {
                    Sell mySell = new Sell
                    {
                        Date = sell.Date,
                        ProductsMachineId = sell.ProductsMachineId,
                        Quantity = sell.Quantity
                    };

                    var response =  await _apiServices.AddModelAsync("http://ehpvendingmachineapi.azurewebsites.net/", "api/", "Sells",
                        mySell);
                }

                _dataServices.DeleteAll<Sell>();
            }
            catch (Exception e)
            {
                InsertUpdateError(true);

                await Application.Current.MainPage.DisplayAlert(
                    "Error Sells",
                    e.Message,
                    "Ok");
            }
        }

        /// <summary>
        /// Metodo para inserir ou atualizar na BD local se existiu erro ao atualizar tabelas e guarda na propriedade UpdateError
        /// </summary>
        /// <param name="UpdateError"></param>
        private void InsertUpdateError(bool UpdateError)
        {
            MainViewModel.GetInstance().UpdateError = UpdateError;

            Update update = new Update
            {
                UpdateId = 1,
                UpdateError = UpdateError
            };

            _dataServices.InsertOrUpdate(update);
        }

        
    }
}
