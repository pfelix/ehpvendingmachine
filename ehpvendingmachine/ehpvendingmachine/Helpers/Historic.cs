﻿using ehpvendingmachine.Models;
using ehpvendingmachine.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ehpvendingmachine.Helpers
{
    public class Historic
    {
        private DataServices _dataServices;

        public Historic()
        {
            _dataServices = new DataServices();
        }
        
        /// <summary>
        /// Cria alerta se stock a 3 ou 0 de quantidade, retorna true
        /// </summary>
        /// <returns></returns>
        public bool CreatAlertStock()
        {
            bool creat = false;

            var productsMachineList = _dataServices.Get<ProductsMachine>(false);

            List<HistoricAlertLocal> alartExist = _dataServices.Get<HistoricAlertLocal>(false);

            foreach (var item in productsMachineList)
            {

                int lowStock = 3;

                // Verifica se está com 3 ou menos do produto
                if (item.ProductQuantity <= lowStock && item.ProductQuantity > 0)
                {
                    int setAlertId = 19;

                    var findAlertExist = alartExist.FirstOrDefault(a => a.MachineId == item.MachineId && a.SetAlertId == setAlertId);

                    if(findAlertExist == null)
                    {
                        HistoricAlertLocal historicAlert = new HistoricAlertLocal
                        {
                            HistoricAlertId = 0,
                            MachineId = item.MachineId,
                            SetAlertId = setAlertId,
                            Date = DateTime.Now
                        };

                        _dataServices.InsertOrUpdate(historicAlert);

                        creat = true;
                    }
                }

                // Verifica se está com 0 do produto

                if (item.ProductQuantity == 0)
                {
                    int setAlertId = 20;

                    var findAlertExist = alartExist.FirstOrDefault(a => a.MachineId == item.MachineId && a.SetAlertId == setAlertId);

                    if (findAlertExist == null)
                    {
                        HistoricAlertLocal historicAlert = new HistoricAlertLocal
                        {
                            HistoricAlertId = 0,
                            MachineId = item.MachineId,
                            SetAlertId = setAlertId,
                            Date = DateTime.Now
                        };

                        _dataServices.InsertOrUpdate(historicAlert);

                        creat = true;
                    }
                }
            }

            return creat;
        }

        /// <summary>
        /// Cria alerta se trocos a 10% ou 0%, retorna true
        /// </summary>
        /// <returns></returns>
        public bool CreatAlertChange()
        {
            bool creat = false;

            var changeList = _dataServices.Get<Change>(false);

            List<HistoricAlertLocal> alartExist = _dataServices.Get<HistoricAlertLocal>(false);

            foreach (var item in changeList)
            {

                int lowStock = (int)(item.QuantityLimit * 0.10);

                // Verifica se está com 10% do produto
                if (item.Quantity <= lowStock && item.Quantity > 0)
                {
                    int setAlertId = 21;

                    var findAlertExist = alartExist.FirstOrDefault(a => a.MachineId == item.MachineId && a.SetAlertId == setAlertId);

                    if (findAlertExist == null)
                    {

                        HistoricAlertLocal historicAlert = new HistoricAlertLocal
                        {
                            HistoricAlertId = 0,
                            MachineId = item.MachineId,
                            SetAlertId = setAlertId,
                            Date = DateTime.Now
                        };

                        _dataServices.InsertOrUpdate(historicAlert);

                        creat = true;
                    }
                }

                // Verifica se está com 0% do produto

                if (item.Quantity == 0)
                {
                    int setAlertId = 22;

                    var findAlertExist = alartExist.FirstOrDefault(a => a.MachineId == item.MachineId && a.SetAlertId == setAlertId);

                    if (findAlertExist == null)
                    {


                        HistoricAlertLocal historicAlert = new HistoricAlertLocal
                        {
                            HistoricAlertId = 0,
                            MachineId = item.MachineId,
                            SetAlertId = setAlertId,
                            Date = DateTime.Now
                        };

                        _dataServices.InsertOrUpdate(historicAlert);

                        creat = true;
                    }
                }
            }

            return creat;
        }

        /// <summary>
        /// Cria alerta se faltar 2 dias ou 0 dias para terminar a validade
        /// </summary>
        /// <returns></returns>
        public bool CreatAlertProductValidity()
        {
            bool creat = false;

            var productsMachineList = _dataServices.Get<ProductsMachine>(false);

            List<HistoricAlertLocal> alartExist = _dataServices.Get<HistoricAlertLocal>(false);

            foreach (var item in productsMachineList)
            {
                DateTime date = DateTime.Now.AddDays(2);

                // Verifica se faltam dois dias
                if (item.ValidityDate <= date && item.ValidityDate > DateTime.Now)
                {
                    int setAlertId = 23;

                    var findAlertExist = alartExist.FirstOrDefault(a => a.MachineId == item.MachineId && a.SetAlertId == setAlertId);

                    if (findAlertExist == null)
                    {
                        HistoricAlertLocal historicAlert = new HistoricAlertLocal
                        {
                            HistoricAlertId = 0,
                            MachineId = item.MachineId,
                            SetAlertId = setAlertId,
                            Date = DateTime.Now
                        };

                        _dataServices.InsertOrUpdate(historicAlert);

                        creat = true;
                    }
                }

                // Verifica se está sem validade
                if (item.ValidityDate <= DateTime.Now)
                {
                    int setAlertId = 24;

                    var findAlertExist = alartExist.FirstOrDefault(a => a.MachineId == item.MachineId && a.SetAlertId == setAlertId);

                    if (findAlertExist == null)
                    {

                        HistoricAlertLocal historicAlert = new HistoricAlertLocal
                        {
                            HistoricAlertId = 0,
                            MachineId = item.MachineId,
                            SetAlertId = setAlertId,
                            Date = DateTime.Now
                        };

                        _dataServices.InsertOrUpdate(historicAlert);

                        creat = true;
                    }
                }
            }

            return creat;
        }

    }
}
