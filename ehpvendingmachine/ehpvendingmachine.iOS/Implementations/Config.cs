﻿// é igual a meter o Add reference
// Ao mesmo tempo está a fazer o pré-compilar, ou seja, cria o dll
[assembly: Xamarin.Forms.Dependency(typeof(ehpvendingmachine.iOS.Implementations.Config))]

namespace ehpvendingmachine.iOS.Implementations
{
    using System;
    using Interfaces;
    using SQLite.Net.Interop;

    public class Config : IConfig
    {
        private string directoryDB;

        private ISQLitePlatform platform;

        public string DirectoryDB
        {
            get
            {
                if (string.IsNullOrEmpty(directoryDB))
                {
                    // Ir buscar a basta onde se cria a base de dados
                    var directory = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
                    directoryDB = System.IO.Path.Combine(directory, "..", "Library");
                }

                return directoryDB;
            }
        }

        public ISQLitePlatform Platform
        {
            get
            {
                if (platform == null)
                {
                    // Ir buscar a plataforma
                    platform = new SQLite.Net.Platform.XamarinIOS.SQLitePlatformIOS();
                }

                return platform;
            }
        }
    }
}