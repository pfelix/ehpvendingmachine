﻿using Newtonsoft.Json;

namespace ehpvendingmachine.ClassLibrary.Models
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public class SetAlert
    {
        [Key]
        [Display(Name = "Set Alert ID")]
        public int SetAlertId { get; set; }

        [Display(Name = "Alert Type")]
        [Required(ErrorMessage = "you must to choose {0}")]
        public int AlertTypeId { get; set; }

        [Display(Name = "Priority")]
        [Required(ErrorMessage = "you must to choose {0}")]
        public int PriorityId { get; set; }

        [Display(Name = "Description")]
        [Required(ErrorMessage = "should indicate a {0}")]
        public string Description { get; set; }

        // Ligação Priority - SetAlert: 1 para muitos
        [JsonIgnore]
        public virtual Priority Priority { get; set; }

        // Ligação AlertType - SetAlert: 1 para muitos
        [JsonIgnore]
        public virtual AlertType AlertType { get; set; }

        // Ligação SetAlert - HistoricAlert: 1 para muitos
        [JsonIgnore]
        public virtual ICollection<HistoricAlert> HistoricAlerts { get; set; }

       
    }
}
