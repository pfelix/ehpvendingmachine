﻿namespace ehpvendingmachine.ClassLibrary.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class RequestMachine
    {
        [Key]
        public int RequestMachineId { get; set; }

        [Display(Name = "User ID")]
        [Required(ErrorMessage = "You must select a {0}")]
        public string UserId { get; set; }

        public bool Assigned { get; set; }
    }
}
