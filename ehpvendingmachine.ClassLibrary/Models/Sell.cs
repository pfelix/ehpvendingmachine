﻿namespace ehpvendingmachine.ClassLibrary.Models
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using Newtonsoft.Json;

    public class Sell
    {
        [Key]
        [Display(Name = "Sell ID")]
        public int SellId { get; set; }

        [Display(Name = "Product Machine ID")]
        [Required(ErrorMessage = "You must to choose {0}")]
        public int ProductsMachineId { get; set; }

        [Display(Name = "Quantity")]
        [Required(ErrorMessage = "should indicate a {0}")]
        [Range(0, 999999, ErrorMessage = "the value must be greater than 0.")]
        public int Quantity { get; set; }

        [Required(ErrorMessage = "should indicate a {0}")]
        [Display(Name = "Date")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime Date { get; set; }

        // Liagação ProductsMachine - Sell : 1 para muitos
        [JsonIgnore]
        public virtual ProductsMachine ProductsMachine { get; set; }    
    }
}
