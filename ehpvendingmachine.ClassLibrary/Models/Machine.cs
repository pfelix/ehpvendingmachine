﻿using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;

namespace ehpvendingmachine.ClassLibrary.Models
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public class Machine
    {
        [Key]
        [Display(Name = "Machine")]
        public int MachineId { get; set; }

        [Display(Name = "Capacity Id")]
        [Required(ErrorMessage = "You must enter a {0}")]
        public int CapacityId { get; set; }

        [NotMapped]
        [JsonIgnore]
        public string MachineIdString { get; set; }

        // Ligação Machine - HistoricAlert: 1 para muitos
        [JsonIgnore]
        public virtual ICollection<HistoricAlert> HistoricAlerts { get; set; }

        // Ligação Machine - Change: 1 para muitos
        [JsonIgnore]
        public virtual ICollection<Change> Changes { get; set; }

        // Ligação Capacity - Machine: 1 para muitos
        [JsonIgnore]
        public virtual Capacity Capacity { get; set; }

        [JsonIgnore]
        public virtual ICollection<AssignMachine> AssignMachines { get; set; }

    }
}
