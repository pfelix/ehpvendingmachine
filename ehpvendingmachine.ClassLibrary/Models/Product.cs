﻿using Newtonsoft.Json;

namespace ehpvendingmachine.ClassLibrary.Models
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public class Product
    {
        [Key]
        [Display(Name = "Produt ID")]
        public int ProductId { get; set; }

        
        [Required(ErrorMessage = "You must insert a {0}")]
        [MaxLength(50, ErrorMessage = "The field {0} only can contain {1} characters.")]
        public string Name { get; set; }

        [Display(Name = "Produt Type")]
        [Required(ErrorMessage = "You must insert a {0}")]
        [Range(1, double.MaxValue, ErrorMessage = "You have to select a {0}")]//minimo selecionavel é 1
        public int ProductTypeId { get; set; }

       
        [Required(ErrorMessage = "You must insert a {0}")]
        public double Price { get; set; }

        [Display(Name = "Product Validity")]
        [Required(ErrorMessage = "You must insert a {0}")]
        public int ValidityDays { get; set; }

        public string Image { get; set; }


        // Ligação ProductType - Product: 1 para muitos
        [JsonIgnore]
        public virtual ProductType ProductType { get; set; }

        // Ligação Product - ProductsMachine: 1 para muitos
        [JsonIgnore]
        public virtual ICollection<ProductsMachine> ProductsMachine { get; set; }

        
    }
}
