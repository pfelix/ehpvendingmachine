﻿using Newtonsoft.Json;

namespace ehpvendingmachine.ClassLibrary.Models
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public class ProductType
    {
        [Key]
        [Display(Name = "Product ID")]
        public int ProductTypeId { get; set; }

        [Display(Name = "Segment")]
        [Required(ErrorMessage = "You must insert a {0}")]
        [MaxLength(50, ErrorMessage = "The field {0} only can contain {1} characters.")]
        [Index("Segment_Index", IsUnique = true)]
        public string Segment { get; set; }

        // Ligação ProductType - Product: 1 para muitos
        [JsonIgnore]
        public virtual ICollection<Product> Products { get; set; }

       
    }
}
