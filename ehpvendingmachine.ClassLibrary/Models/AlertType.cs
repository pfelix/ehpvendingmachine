﻿using Newtonsoft.Json;

namespace ehpvendingmachine.ClassLibrary.Models
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public class AlertType
    {
        [Key]
        [Display(Name = "Alert Type ID")]
        public int AlertTypeId { get; set; }

        [Display(Name = "Type Alert")]
        [StringLength(50, ErrorMessage = "The {0} only can contain {1} characters.")]
        [Required(ErrorMessage = "Deve inserir um {0}")]
        [MaxLength(50, ErrorMessage = "The field {0} only can contain {1} characters.")]
        [Index("AlertType_Name_Index", IsUnique = true)]
        public string Name { get; set; }

        [InverseProperty("AlertType")]
        [JsonIgnore]
        // Ligação AlertType - SetAlert: 1 para muitos
        public virtual ICollection<SetAlert> SetAlerts { get; set; }
       
    }
}
