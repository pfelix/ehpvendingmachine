﻿using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;

namespace ehpvendingmachine.ClassLibrary.Models
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public class Capacity
    {
        private string _nameQuantity;

        [Key]
        [Display(Name = "Capacity Id")]
        public int CapacityId { get; set; }

        [Required(ErrorMessage = "You must enter a {0}")]
        [MaxLength(50, ErrorMessage = "The field {0} only can contain {1} characters.")]
        public string Name { get; set; }

        [Required(ErrorMessage = "You must enter a {0}")]
        [Range(0,999999, ErrorMessage = "the value must be greater than 0.")]
        public int Quantity { get; set; }

        // Ligação Capacity - Machine: 1 para muitos
        [JsonIgnore]
        public virtual ICollection<Machine> Machines { get; set; }

       

    }
}
