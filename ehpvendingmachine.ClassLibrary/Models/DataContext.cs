namespace ehpvendingmachine.ClassLibrary.Models
{
    using System.Data.Entity;
    using System.Data.Entity.ModelConfiguration.Conventions;

    public class DataContext:DbContext
    {
        public DataContext():base("DefaultConnection")
        {
            
        }

        // Para retirar a predefinição de Cascade on delete
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
        }

        #region Machines

        public System.Data.Entity.DbSet<ehpvendingmachine.ClassLibrary.Models.Machine> Machines { get; set; }

        public System.Data.Entity.DbSet<ehpvendingmachine.ClassLibrary.Models.Capacity> Capacities { get; set; }

        public System.Data.Entity.DbSet<ehpvendingmachine.ClassLibrary.Models.CurrencyValue> CurrencyValues { get; set; }

        public System.Data.Entity.DbSet<ehpvendingmachine.ClassLibrary.Models.Change> Changes { get; set; }

        public System.Data.Entity.DbSet<ehpvendingmachine.ClassLibrary.Models.AssignMachine> AssignMachines { get; set; }

        public System.Data.Entity.DbSet<ehpvendingmachine.ClassLibrary.Models.ProductsMachine> ProductsMachines { get; set; }

        public System.Data.Entity.DbSet<ehpvendingmachine.ClassLibrary.Models.RequestMachine> RequestMachines { get; set; }

        #endregion

        #region Products

        public System.Data.Entity.DbSet<ehpvendingmachine.ClassLibrary.Models.ProductType> ProductTypes { get; set; }

        public System.Data.Entity.DbSet<ehpvendingmachine.ClassLibrary.Models.Product> Products { get; set; }

        public System.Data.Entity.DbSet<ehpvendingmachine.ClassLibrary.Models.Sell> Sells { get; set; }

        #endregion

        #region Alerts

        public System.Data.Entity.DbSet<ehpvendingmachine.ClassLibrary.Models.SetAlert> SetAlerts { get; set; }

        public System.Data.Entity.DbSet<ehpvendingmachine.ClassLibrary.Models.AlertType> AlertTypes { get; set; }

        public System.Data.Entity.DbSet<ehpvendingmachine.ClassLibrary.Models.Priority> Priorities { get; set; }

        public System.Data.Entity.DbSet<ehpvendingmachine.ClassLibrary.Models.HistoricAlert> HistoricAlerts { get; set; }

        #endregion

    }
}
