using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;

namespace ehpvendingmachine.ClassLibrary.Models
{
    using System;
    using System.ComponentModel.DataAnnotations;

    public class AssignMachine
    {
        [Key]
        [Display(Name = "Assign Machine ID")]
        public int AssignMachineId { get; set; }

        [Display(Name = "User")]
        [Required(ErrorMessage = "You must select a {0}")]
        public string UserId { get; set; }

        [Display(Name = "Machine")]
        [Required(ErrorMessage = "You must select a {0}")]
        public int MachineId { get; set; }

        [Display(Name = "Date Assignment")]
        [Required(ErrorMessage = "You must insert a {0}")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime AssignDate { get; set; }

        [NotMapped]
        [JsonIgnore]
        public string UserName { get; set; }

        // Ligação AssignMachine - Machine: 1 para muitos
        [JsonIgnore]
        public virtual Machine Machine { get; set; }
        

        
    }
}
