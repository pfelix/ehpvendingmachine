﻿using Newtonsoft.Json;

namespace ehpvendingmachine.ClassLibrary.Models
{
    using System;
    using System.ComponentModel.DataAnnotations;

    public class HistoricAlert
    {
        [Key]
        [Display(Name = "Historic Alert ID")]
        public int HistoricAlertId { get; set; }

        [Display(Name = "Machine ID")]
        [Required(ErrorMessage = "you must to choose {0}")]
        public int MachineId { get; set; }

        [Display(Name = "Alert ID")]
        [Required(ErrorMessage = "you must to choose {0}")]
        public int SetAlertId { get; set; }
       
        [Display(Name = "Date")]
        [Required(ErrorMessage = "should indicate a {0}")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime Date { get; set; }

        [Display(Name = "Resolution Date")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? ResolutionDate { get; set; }

        // Ligação SetAlert - HistoricAlert: 1 para muitos
        [JsonIgnore]
        public virtual SetAlert SetAlert { get; set; }

        // Ligação Machine - HistoricAlert: 1 para muitos
        [JsonIgnore]
        public virtual Machine Machine { get; set; }

       

    }
}
