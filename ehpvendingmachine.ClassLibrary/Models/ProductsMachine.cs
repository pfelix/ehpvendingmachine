﻿namespace ehpvendingmachine.ClassLibrary.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using Newtonsoft.Json;

    public class ProductsMachine
    {
        [Key]
        [Display(Name = "Product Machine ID")]
        public int ProductsMachineId { get; set; }

        [Display(Name = "Machine")]
        [Required(ErrorMessage = "You must select a {0}")]
        public int MachineId { get; set; }

        [Display(Name = "Product ID")]
        [Required(ErrorMessage = "You must select a {0}")]
        public int ProductId { get; set; }

        [Display(Name = "Validity")]
        [Required(ErrorMessage = "You must insert a {0}")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime ValidityDate { get; set; }

        [Display(Name = "Quantity")]
        [Required(ErrorMessage = "You must insert a {0}")]
        [Range(0, 999999, ErrorMessage = "the value must be greater than 0.")]
        public int ProductQuantity { get; set; }

        [Display(Name = "Altered?")]
        public bool Altered { get; set; } = false;

        // Ligação ProductsMachine - Sell: 1 para muitos
        [JsonIgnore]
        public virtual ICollection<Sell> Sells { get; set; }

        // Ligação Machine - ProductsMachine: 1 para muitos
        [JsonIgnore]
        public virtual Machine Machine { get; set; }

        // Ligação Product - ProductsMachine: 1 para muitos
        [JsonIgnore]
        public virtual Product Product { get; set; }

        [NotMapped]
        public string Name { get; set; }

        [NotMapped]
        public double Price { get; set; }


    }
}
