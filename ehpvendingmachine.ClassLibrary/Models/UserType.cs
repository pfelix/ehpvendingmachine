﻿namespace ehpvendingmachine.ClassLibrary.Models
{
    public enum UserType
    {
       Administrator,

       User,

       Client
    }
}
