﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;

namespace ehpvendingmachine.ClassLibrary.Models
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public class CurrencyValue
    {
        [Key]
        [Display(Name = "Currency Value Id")]
        public int CurrencyValueId { get; set; }

        [Display(Name = "Currency")]
        [Required(ErrorMessage = "You must enter a {0}")]
        [DisplayFormat(DataFormatString = "{0:C2}", ApplyFormatInEditMode = false)]
        [Index("CurrencyValue_Currency", IsUnique = true)]
        [Range(0, 999999, ErrorMessage = "the value must be greater than 0.")]
        public double Currency { get; set; }

        [NotMapped]
        [Display(Name = "Currency")]
        public string CurrencyName { get; set; }

        // Ligação CurrencyValue - Change: 1 para muitos
        [JsonIgnore]
        public virtual ICollection<Change> Changes { get; set; }

       
    }
}
