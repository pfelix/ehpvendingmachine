﻿namespace ehpvendingmachine.ClassLibrary.Models
{
    public class UserModel
    {
        
        public string UserId { get; set; }
        
        public string FirstName { get; set; }
        
        public string LastName { get; set; }
        
        public string UserName { get; set; }
        
        public string Adress { get; set; }
        
        public string Location { get; set; }
        
        public int TIN { get; set; }
        
        public string Email { get; set; }
        
        public string PhoneNumber { get; set; }
        
        public string UserType { get; set; }

        
    }
}
