﻿using Newtonsoft.Json;

namespace ehpvendingmachine.ClassLibrary.Models
{
    using System.ComponentModel.DataAnnotations;


    public class Change
    {
        [Key]
        [Display(Name = "Change Id")]
        public int ChangeId { get; set; }

        [Display(Name = "Currency Value Id")]
        [Required(ErrorMessage = "You must enter a {0}")]
        public int CurrencyValueId { get; set; }

        [Display(Name = "Machine")]
        [Required(ErrorMessage = "You must enter a {0}")]
        public int MachineId { get; set; }

        [Required(ErrorMessage = "You must enter a {0}")]
        [Range(0, 999999, ErrorMessage = "the value must be greater than 0.")]
        public int Quantity { get; set; }

        [Display(Name = "Quantity Limit")]
        [Required(ErrorMessage = "You must enter a {0}")]
        [Range(0, 999999, ErrorMessage = "the value must be greater than 0.")]
        public int QuantityLimit { get; set; }

        [Display(Name = "Altered?")]
        public bool Altered { get; set; }

        // Ligação CurrencyValue - Change: 1 para muitos
        [JsonIgnore]
        public virtual CurrencyValue CurrencyValue { get; set; }

        // Ligação Machine - Change: 1 para muitos
        [JsonIgnore]
        public virtual Machine Machine { get; set; }

        

    }
}
