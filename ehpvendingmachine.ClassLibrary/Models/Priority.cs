﻿using Newtonsoft.Json;

namespace ehpvendingmachine.ClassLibrary.Models
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public class Priority
    {
        [Key]
        [Display(Name = "Priority ID")]
        public int PriorityId { get; set; }

        [Display(Name = "Priority Level")]
        [StringLength(50, ErrorMessage = "O {0} não pode ter mais que {1} carateres")]
        [Required(ErrorMessage = "should indicate a {0}")]
        public string Name { get; set; }

        // Ligação Priority - SetAlert: 1 para muitos
        [JsonIgnore]
        public virtual ICollection<SetAlert> SetAlerts { get; set; }

       
    }
}
