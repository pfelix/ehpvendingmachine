﻿namespace ehpvendingmachine.ClassLibrary.Models
{
    public class DashBoard
    {     
        public int ProductCounted { get; set; }

        public int MachineCounted { get; set; }

        public int AssignedMachineCounted { get; set; }

        public int SellsCounted { get; set; }

        public int AlertCounted { get; set; }

        public int HistorcAlertCounted { get; set; }

        public int MachinesRequestedCounted { get; set; }

        public int ProductMachinesCounted { get; set; }
    }
}
