﻿using ehpvendingmachine.ClassLibrary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ehpvendingmachine.ClassLibrary.ViewModels
{
    public class HistoricAlertsViewModel
    {
        public HistoricAlert HistoricAlert { get; set; }

        public SetAlert SetAlert { get; set; }

        public Priority Priority { get; set; }

        public AlertType AlertType { get; set; }

    }
}
