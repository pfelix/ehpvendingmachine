﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ehpvendingmachine.ClassLibrary.Models;

namespace ehpvendingmachine.ClassLibrary.ViewModels
{
    public class ProductReloadViewModel
    {
        public ProductsMachine ProductsMachine { get; set; }

        public List<ProductsMachine> ProductsMachines { get; set; }

        //public Capacity Capacity { get; set; }

        //public List<Capacity> Capacities { get; set; }

        public List<Machine> Machines { get; set; }

    }
}
