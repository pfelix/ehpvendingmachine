﻿using ehpvendingmachine.ClassLibrary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ehpvendingmachine.ClassLibrary.ViewModels
{
    public class SellsViewModel
    {
        public Sell Sell { get; set; }

        public Product Product { get; set; }

        public Machine Machine { get; set; }

        public double Value { get; set; }
    }
}
