﻿using ehpvendingmachine.MVC.Helpers;

namespace ehpvendingmachine.MVC
{
    using ehpvendingmachine.ClassLibrary.Models;
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using System.Web;
    using System.Web.Mvc;
    using System.Web.Optimization;
    using System.Web.Routing;

    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            Database.SetInitializer(
                new MigrateDatabaseToLatestVersion<Models.DataContextLocal,
                Migrations.Configuration>());

            //context da autenticacaco
            ApplicationDbContext db = new ApplicationDbContext();

            //criar os papeis para os users
            CreateRoles(db);

            //para criar o administrador, que gere tudo
            CreateSuperUser(db);

            AddpermitionsToSuperUser(db);

            db.Dispose();//fechar basedados


            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }

        /// <summary>
        /// dar permissoes ao super utilizador
        /// </summary>
        /// <param name="db"></param>
        private void AddpermitionsToSuperUser(ApplicationDbContext db)
        {
            // Objeto para manipular user
            var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(db));

            var user = userManager.FindByName("sa@ehpvendingmachine.com");

            AddsHelpers.Addpermitions(user.Id, userManager);
        }

        /// <summary>
        /// metodo para criar o super utilizador
        /// </summary>
        /// <param name="db"></param>
        private void CreateSuperUser(ApplicationDbContext db)
        {
            var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(db));

            var user = userManager.FindByName("sa@ehpvendingmachine.com");

            if (user == null)
            {
                user = new ApplicationUser
                {
                    UserName = "sa@ehpvendingmachine.com",
                    Email = "sa@ehpvendingmachine.com",
                    UserType = UserType.Administrator.ToString()
                };

                //password para o user
                userManager.Create(user, "P@ssw0rd");
            }
        }

        /// <summary>
        /// criar as roles
        /// </summary>
        /// <param name="db"></param>
        private void CreateRoles(ApplicationDbContext db)
        {
            // Objeto para manipular as Roles
            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(db));

            #region Machines Group

            // Role Geral Machines Group
            if (!roleManager.RoleExists("Machines Group"))
            {
                roleManager.Create(new IdentityRole("Machines Group"));
            }

            // Roles CurrencyValues
            if (!roleManager.RoleExists("CurrencyValues"))
            {
                roleManager.Create(new IdentityRole("CurrencyValues"));
            }

            if (!roleManager.RoleExists("CurrencyValues View"))
            {
                roleManager.Create(new IdentityRole("CurrencyValues View"));
            }

            if (!roleManager.RoleExists("CurrencyValues Create"))
            {
                roleManager.Create(new IdentityRole("CurrencyValues Create"));
            }

            if (!roleManager.RoleExists("CurrencyValues Delete"))
            {
                roleManager.Create(new IdentityRole("CurrencyValues Delete"));
            }

            // Roles Changes
            if (!roleManager.RoleExists("Changes"))
            {
                roleManager.Create(new IdentityRole("Changes"));
            }

            if (!roleManager.RoleExists("Changes View"))
            {
                roleManager.Create(new IdentityRole("Changes View"));
            }

            if (!roleManager.RoleExists("Changes Create"))
            {
                roleManager.Create(new IdentityRole("Changes Create"));
            }

            if (!roleManager.RoleExists("Changes Delete"))
            {
                roleManager.Create(new IdentityRole("Changes Delete"));
            }

            if (!roleManager.RoleExists("Changes Edit"))
            {
                roleManager.Create(new IdentityRole("Changes Edit"));
            }

            // Roles ChangeReloads
            if (!roleManager.RoleExists("ChangeReloads"))
            {
                roleManager.Create(new IdentityRole("ChangeReloads"));
            }

            if (!roleManager.RoleExists("ChangeReloads View"))
            {
                roleManager.Create(new IdentityRole("ChangeReloads View"));
            }

            if (!roleManager.RoleExists("ChangeReloads Edit"))
            {
                roleManager.Create(new IdentityRole("ChangeReloads Edit"));
            }

            // Roles Capacities
            if (!roleManager.RoleExists("Capacities"))
            {
                roleManager.Create(new IdentityRole("Capacities"));
            }

            if (!roleManager.RoleExists("Capacities View"))
            {
                roleManager.Create(new IdentityRole("Capacities View"));
            }

            if (!roleManager.RoleExists("Capacities Create"))
            {
                roleManager.Create(new IdentityRole("Capacities Create"));
            }

            if (!roleManager.RoleExists("Capacities Edit"))
            {
                roleManager.Create(new IdentityRole("Capacities Edit"));
            }

            if (!roleManager.RoleExists("Capacities Delete"))
            {
                roleManager.Create(new IdentityRole("Capacities Delete"));
            }

            // Roles Machines
            if (!roleManager.RoleExists("Machines"))
            {
                roleManager.Create(new IdentityRole("Machines"));
            }

            if (!roleManager.RoleExists("Machines View"))
            {
                roleManager.Create(new IdentityRole("Machines View"));
            }

            if (!roleManager.RoleExists("Machines Create"))
            {
                roleManager.Create(new IdentityRole("Machines Create"));
            }

            if (!roleManager.RoleExists("Machines Edit"))
            {
                roleManager.Create(new IdentityRole("Machines Edit"));
            }

            if (!roleManager.RoleExists("Machines Delete"))
            {
                roleManager.Create(new IdentityRole("Machines Delete"));
            }

            // Roles ProductsMachine
            if (!roleManager.RoleExists("ProductsMachine"))
            {
                roleManager.Create(new IdentityRole("ProductsMachine"));
            }

            if (!roleManager.RoleExists("ProductsMachine View"))
            {
                roleManager.Create(new IdentityRole("ProductsMachine View"));
            }

            if (!roleManager.RoleExists("ProductsMachine Create"))
            {
                roleManager.Create(new IdentityRole("ProductsMachine Create"));
            }

            if (!roleManager.RoleExists("ProductsMachine Delete"))
            {
                roleManager.Create(new IdentityRole("ProductsMachine Delete"));
            }

            // Roles ProductsMachine Reload
            if (!roleManager.RoleExists("ProductsMachine Reload"))
            {
                roleManager.Create(new IdentityRole("ProductsMachine Reload"));
            }

            if (!roleManager.RoleExists("ProductsMachine Reload View"))
            {
                roleManager.Create(new IdentityRole("ProductsMachine Reload View"));
            }

            if (!roleManager.RoleExists("ProductsMachine Reload Add"))
            {
                roleManager.Create(new IdentityRole("ProductsMachine Reload Add"));
            }

            // Roles AssignMachines
            if (!roleManager.RoleExists("AssignMachines"))
            {
                roleManager.Create(new IdentityRole("AssignMachines"));
            }

            if (!roleManager.RoleExists("AssignMachines View"))
            {
                roleManager.Create(new IdentityRole("AssignMachines View"));
            }

            if (!roleManager.RoleExists("AssignMachines Create"))
            {
                roleManager.Create(new IdentityRole("AssignMachines Create"));
            }

            if (!roleManager.RoleExists("AssignMachines Delete"))
            {
                roleManager.Create(new IdentityRole("AssignMachines Delete"));
            }

            #endregion

            #region Alerts Group

            // Role Geral Alerts Group
            if (!roleManager.RoleExists("Alerts Group"))
            {
                roleManager.Create(new IdentityRole("Alerts Group"));
            }

            // Roles SetAlerts
            if (!roleManager.RoleExists("SetAlerts"))
            {
                roleManager.Create(new IdentityRole("SetAlerts"));
            }

            if (!roleManager.RoleExists("SetAlerts View"))
            {
                roleManager.Create(new IdentityRole("SetAlerts View"));
            }

            if (!roleManager.RoleExists("SetAlerts Create"))
            {
                roleManager.Create(new IdentityRole("SetAlerts Create"));
            }

            if (!roleManager.RoleExists("SetAlerts Edit"))
            {
                roleManager.Create(new IdentityRole("SetAlerts Edit"));
            }

            if (!roleManager.RoleExists("SetAlerts Delete"))
            {
                roleManager.Create(new IdentityRole("SetAlerts Delete"));
            }

            // Roles Priorities
            if (!roleManager.RoleExists("Priorities"))
            {
                roleManager.Create(new IdentityRole("Priorities"));
            }

            if (!roleManager.RoleExists("Priorities View"))
            {
                roleManager.Create(new IdentityRole("Priorities View"));
            }

            if (!roleManager.RoleExists("Priorities Create"))
            {
                roleManager.Create(new IdentityRole("Priorities Create"));
            }

            if (!roleManager.RoleExists("Priorities Edit"))
            {
                roleManager.Create(new IdentityRole("Priorities Edit"));
            }

            if (!roleManager.RoleExists("Priorities Delete"))
            {
                roleManager.Create(new IdentityRole("Priorities Delete"));
            }

            // Roles AlertTypes
            if (!roleManager.RoleExists("AlertTypes"))
            {
                roleManager.Create(new IdentityRole("AlertTypes"));
            }

            if (!roleManager.RoleExists("AlertTypes View"))
            {
                roleManager.Create(new IdentityRole("AlertTypes View"));
            }

            if (!roleManager.RoleExists("AlertTypes Create"))
            {
                roleManager.Create(new IdentityRole("AlertTypes Create"));
            }

            if (!roleManager.RoleExists("AlertTypes Edit"))
            {
                roleManager.Create(new IdentityRole("AlertTypes Edit"));
            }

            if (!roleManager.RoleExists("AlertTypes Delete"))
            {
                roleManager.Create(new IdentityRole("AlertTypes Delete"));
            }

            // Roles HistoricAlerts
            if (!roleManager.RoleExists("HistoricAlerts"))
            {
                roleManager.Create(new IdentityRole("HistoricAlerts"));
            }

            if (!roleManager.RoleExists("HistoricAlerts View"))
            {
                roleManager.Create(new IdentityRole("HistoricAlerts View"));
            }

            if (!roleManager.RoleExists("HistoricAlerts Edit"))
            {
                roleManager.Create(new IdentityRole("HistoricAlerts Edit"));
            }

            #endregion

            #region Products Group

            // Role Geral Products Group
            if (!roleManager.RoleExists("Products Group"))
            {
                roleManager.Create(new IdentityRole("Products Group"));
            }

            // Roles ProductTypes
            if (!roleManager.RoleExists("ProductTypes"))
            {
                roleManager.Create(new IdentityRole("ProductTypes"));
            }

            if (!roleManager.RoleExists("ProductTypes View"))
            {
                roleManager.Create(new IdentityRole("ProductTypes View"));
            }

            if (!roleManager.RoleExists("ProductTypes Create"))
            {
                roleManager.Create(new IdentityRole("ProductTypes Create"));
            }

            if (!roleManager.RoleExists("ProductTypes Edit"))
            {
                roleManager.Create(new IdentityRole("ProductTypes Edit"));
            }

            if (!roleManager.RoleExists("ProductTypes Delete"))
            {
                roleManager.Create(new IdentityRole("ProductTypes Delete"));
            }

            // Roles Products
            if (!roleManager.RoleExists("Products"))
            {
                roleManager.Create(new IdentityRole("Products"));
            }

            if (!roleManager.RoleExists("Products View"))
            {
                roleManager.Create(new IdentityRole("Products View"));
            }

            if (!roleManager.RoleExists("Products Create"))
            {
                roleManager.Create(new IdentityRole("Products Create"));
            }

            if (!roleManager.RoleExists("Products Edit"))
            {
                roleManager.Create(new IdentityRole("Products Edit"));
            }

            if (!roleManager.RoleExists("Products Delete"))
            {
                roleManager.Create(new IdentityRole("Products Delete"));
            }

            #endregion

            #region Users Group

            // Roles Users
            if (!roleManager.RoleExists("Users"))
            {
                roleManager.Create(new IdentityRole("Users"));
            }

            if (!roleManager.RoleExists("Users View"))
            {
                roleManager.Create(new IdentityRole("Users View"));
            }

            if (!roleManager.RoleExists("Users Create"))
            {
                roleManager.Create(new IdentityRole("Users Create"));
            }

            if (!roleManager.RoleExists("Users Edit"))
            {
                roleManager.Create(new IdentityRole("Users Edit"));
            }

            if (!roleManager.RoleExists("Users Delete"))
            {
                roleManager.Create(new IdentityRole("Users Delete"));
            }

            #endregion

            #region Sells Group

            // Roles Sells
            if (!roleManager.RoleExists("Sells"))
            {
                roleManager.Create(new IdentityRole("Sells"));
            }

            if (!roleManager.RoleExists("Sells View"))
            {
                roleManager.Create(new IdentityRole("Sells View"));
            }

            if (!roleManager.RoleExists("Sells Create"))
            {
                roleManager.Create(new IdentityRole("Sells Create"));
            }

            if (!roleManager.RoleExists("Sells Edit"))
            {
                roleManager.Create(new IdentityRole("Sells Edit"));
            }

            if (!roleManager.RoleExists("Sells Delete"))
            {
                roleManager.Create(new IdentityRole("Sells Delete"));
            }

            #endregion

            #region General group

            if (!roleManager.RoleExists("Admin"))
            {
                roleManager.Create(new IdentityRole("Admin"));
            }

            if (!roleManager.RoleExists("Client"))
            {
                roleManager.Create(new IdentityRole("Client"));
            }

            if (!roleManager.RoleExists("User"))
            {
                roleManager.Create(new IdentityRole("User"));
            }

            #endregion

        }
    }
}
