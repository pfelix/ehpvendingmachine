﻿namespace ehpvendingmachine.MVC.Helpers
{
    using ClassLibrary.Models;
    using Microsoft.AspNet.Identity;
    using System.Linq;

    public class AddsHelpers
    {
        /// <summary>
        /// Metodo para Adicionar Roles ao utilizar quando for associado a um cliente
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="db"></param>
        public static void Addpermitions(string userId, UserManager<ApplicationUser> userManager)
        {
            var users = userManager.Users.ToList();
            var user = users.Find(u => u.Id == userId);

            // Não existe utilizador
            if (user == null)
            {
                return;
            }

            // Permissões para Clientes
            if (user.UserType == "Client")
            {

                #region Users Group

                if (!userManager.IsInRole(user.Id, "Users"))
                {
                    userManager.AddToRole(user.Id, "Users");
                }

                if (!userManager.IsInRole(user.Id, "Users Edit"))
                {
                    userManager.AddToRole(user.Id, "Users Edit");
                }

                #endregion

                #region General Group

                // Role Client geral
                if (!userManager.IsInRole(user.Id, "Client"))
                {
                    userManager.AddToRole(user.Id, "Client");
                }

                #endregion
            }

            // Permissões para Utilizadores
            if (user.UserType == "User")
            {
                #region Machines Group

                if (!userManager.IsInRole(user.Id, "Machines Group"))
                {
                    userManager.AddToRole(user.Id, "Machines Group");
                }

                // Roles Machines
                if (!userManager.IsInRole(user.Id, "Machines"))
                {
                    userManager.AddToRole(user.Id, "Machines");
                }

                if (!userManager.IsInRole(user.Id, "Machines View"))
                {
                    userManager.AddToRole(user.Id, "Machines View");
                }

                // Roles ProductsMachine Reload
                if (!userManager.IsInRole(user.Id, "ProductsMachine Reload"))
                {
                    userManager.AddToRole(user.Id, "ProductsMachine Reload");
                }

                if (!userManager.IsInRole(user.Id, "ProductsMachine Reload View"))
                {
                    userManager.AddToRole(user.Id, "ProductsMachine Reload View");
                }

                if (!userManager.IsInRole(user.Id, "ProductsMachine Reload Add"))
                {
                    userManager.AddToRole(user.Id, "ProductsMachine Reload Add");
                }

                // Roles Change Reloads
                if (!userManager.IsInRole(user.Id, "ChangeReloads"))
                {
                    userManager.AddToRole(user.Id, "ChangeReloads");
                }

                if (!userManager.IsInRole(user.Id, "ChangeReloads View"))
                {
                    userManager.AddToRole(user.Id, "ChangeReloads View");
                }

                if (!userManager.IsInRole(user.Id, "ChangeReloads Edit"))
                {
                    userManager.AddToRole(user.Id, "ChangeReloads Edit");
                }

                // Roles AssignMachines
                if (!userManager.IsInRole(user.Id, "AssignMachines"))
                {
                    userManager.AddToRole(user.Id, "AssignMachines");
                }

                if (!userManager.IsInRole(user.Id, "AssignMachines View"))
                {
                    userManager.AddToRole(user.Id, "AssignMachines View");
                }

                if (!userManager.IsInRole(user.Id, "AssignMachines Create"))
                {
                    userManager.AddToRole(user.Id, "AssignMachines Create");
                }

                if (!userManager.IsInRole(user.Id, "AssignMachines Delete"))
                {
                    userManager.AddToRole(user.Id, "AssignMachines Delete");
                }

                #endregion

                #region Alerts Group

                if (!userManager.IsInRole(user.Id, "Alerts Group"))
                {
                    userManager.AddToRole(user.Id, "Alerts Group");
                }

                // Roles SetAlerts
                if (!userManager.IsInRole(user.Id, "SetAlerts"))
                {
                    userManager.AddToRole(user.Id, "SetAlerts");
                }

                if (!userManager.IsInRole(user.Id, "SetAlerts View"))
                {
                    userManager.AddToRole(user.Id, "SetAlerts View");
                }

                // Roles HistoricAlerts
                if (!userManager.IsInRole(user.Id, "HistoricAlerts"))
                {
                    userManager.AddToRole(user.Id, "HistoricAlerts");
                }

                if (!userManager.IsInRole(user.Id, "HistoricAlerts View"))
                {
                    userManager.AddToRole(user.Id, "HistoricAlerts View");
                }

                if (!userManager.IsInRole(user.Id, "HistoricAlerts Edit"))
                {
                    userManager.AddToRole(user.Id, "HistoricAlerts Edit");
                }

                // Roles ChangeReloads
                if (!userManager.IsInRole(user.Id, "ChangeReloads"))
                {
                    userManager.AddToRole(user.Id, "ChangeReloads");
                }

                if (!userManager.IsInRole(user.Id, "ChangeReloads View"))
                {
                    userManager.AddToRole(user.Id, "ChangeReloads View");
                }

                if (!userManager.IsInRole(user.Id, "ChangeReloads Edit"))
                {
                    userManager.AddToRole(user.Id, "ChangeReloads Edit");
                }

                #endregion

                #region General Group

                // Role User geral
                if (!userManager.IsInRole(user.Id, "User"))
                {
                    userManager.AddToRole(user.Id, "User");
                }

                #endregion

            }

            // Permissões para Administrador
            if (user.UserType == "Administrator")
            {
                #region Machines Group

                if (!userManager.IsInRole(user.Id, "Machines Group"))
                {
                    userManager.AddToRole(user.Id, "Machines Group");
                }

                // Roles CurrencyValues
                if (!userManager.IsInRole(user.Id, "CurrencyValues"))
                {
                    userManager.AddToRole(user.Id, "CurrencyValues");
                }

                if (!userManager.IsInRole(user.Id, "CurrencyValues View"))
                {
                    userManager.AddToRole(user.Id, "CurrencyValues View");
                }

                if (!userManager.IsInRole(user.Id, "CurrencyValues Create"))
                {
                    userManager.AddToRole(user.Id, "CurrencyValues Create");
                }

                if (!userManager.IsInRole(user.Id, "CurrencyValues Delete"))
                {
                    userManager.AddToRole(user.Id, "CurrencyValues Delete");
                }

                // Roles Changes
                if (!userManager.IsInRole(user.Id, "Changes"))
                {
                    userManager.AddToRole(user.Id, "Changes");
                }

                if (!userManager.IsInRole(user.Id, "Changes View"))
                {
                    userManager.AddToRole(user.Id, "Changes View");
                }

                if (!userManager.IsInRole(user.Id, "Changes Create"))
                {
                    userManager.AddToRole(user.Id, "Changes Create");
                }

                if (!userManager.IsInRole(user.Id, "Changes Delete"))
                {
                    userManager.AddToRole(user.Id, "Changes Delete");
                }

                if (!userManager.IsInRole(user.Id, "Changes Edit"))
                {
                    userManager.AddToRole(user.Id, "Changes Edit");
                }

                // Roles Capacities
                if (!userManager.IsInRole(user.Id, "Capacities"))
                {
                    userManager.AddToRole(user.Id, "Capacities");
                }

                if (!userManager.IsInRole(user.Id, "Capacities View"))
                {
                    userManager.AddToRole(user.Id, "Capacities View");
                }

                if (!userManager.IsInRole(user.Id, "Capacities Create"))
                {
                    userManager.AddToRole(user.Id, "Capacities Create");
                }

                if (!userManager.IsInRole(user.Id, "Capacities Delete"))
                {
                    userManager.AddToRole(user.Id, "Capacities Delete");
                }

                if (!userManager.IsInRole(user.Id, "Capacities Edit"))
                {
                    userManager.AddToRole(user.Id, "Capacities Edit");
                }

                // Roles Machines
                if (!userManager.IsInRole(user.Id, "Machines"))
                {
                    userManager.AddToRole(user.Id, "Machines");
                }

                if (!userManager.IsInRole(user.Id, "Machines View"))
                {
                    userManager.AddToRole(user.Id, "Machines View");
                }

                if (!userManager.IsInRole(user.Id, "Machines Create"))
                {
                    userManager.AddToRole(user.Id, "Machines Create");
                }

                if (!userManager.IsInRole(user.Id, "Machines Delete"))
                {
                    userManager.AddToRole(user.Id, "Machines Delete");
                }

                if (!userManager.IsInRole(user.Id, "Machines Edit"))
                {
                    userManager.AddToRole(user.Id, "Machines Edit");
                }

                // Roles ProductsMachine
                if (!userManager.IsInRole(user.Id, "ProductsMachine"))
                {
                    userManager.AddToRole(user.Id, "ProductsMachine");
                }

                if (!userManager.IsInRole(user.Id, "ProductsMachine View"))
                {
                    userManager.AddToRole(user.Id, "ProductsMachine View");
                }

                if (!userManager.IsInRole(user.Id, "ProductsMachine Create"))
                {
                    userManager.AddToRole(user.Id, "ProductsMachine Create");
                }

                if (!userManager.IsInRole(user.Id, "ProductsMachine Delete"))
                {
                    userManager.AddToRole(user.Id, "ProductsMachine Delete");
                }

                // Roles AssignMachines
                if (!userManager.IsInRole(user.Id, "AssignMachines"))
                {
                    userManager.AddToRole(user.Id, "AssignMachines");
                }

                if (!userManager.IsInRole(user.Id, "AssignMachines View"))
                {
                    userManager.AddToRole(user.Id, "AssignMachines View");
                }

                #endregion

                #region Alerts Group

                if (!userManager.IsInRole(user.Id, "Alerts Group"))
                {
                    userManager.AddToRole(user.Id, "Alerts Group");
                }

                // Roles SetAlerts
                if (!userManager.IsInRole(user.Id, "SetAlerts"))
                {
                    userManager.AddToRole(user.Id, "SetAlerts");
                }

                if (!userManager.IsInRole(user.Id, "SetAlerts View"))
                {
                    userManager.AddToRole(user.Id, "SetAlerts View");
                }

                if (!userManager.IsInRole(user.Id, "SetAlerts Create"))
                {
                    userManager.AddToRole(user.Id, "SetAlerts Create");
                }

                if (!userManager.IsInRole(user.Id, "SetAlerts Delete"))
                {
                    userManager.AddToRole(user.Id, "SetAlerts Delete");
                }

                if (!userManager.IsInRole(user.Id, "SetAlerts Edit"))
                {
                    userManager.AddToRole(user.Id, "SetAlerts Edit");
                }

                // Roles Priorities
                if (!userManager.IsInRole(user.Id, "Priorities"))
                {
                    userManager.AddToRole(user.Id, "Priorities");
                }

                if (!userManager.IsInRole(user.Id, "Priorities View"))
                {
                    userManager.AddToRole(user.Id, "Priorities View");
                }

                if (!userManager.IsInRole(user.Id, "Priorities Create"))
                {
                    userManager.AddToRole(user.Id, "Priorities Create");
                }

                if (!userManager.IsInRole(user.Id, "Priorities Delete"))
                {
                    userManager.AddToRole(user.Id, "Priorities Delete");
                }

                if (!userManager.IsInRole(user.Id, "Priorities Edit"))
                {
                    userManager.AddToRole(user.Id, "Priorities Edit");
                }

                // Roles AlertTypes
                if (!userManager.IsInRole(user.Id, "AlertTypes"))
                {
                    userManager.AddToRole(user.Id, "AlertTypes");
                }

                if (!userManager.IsInRole(user.Id, "AlertTypes View"))
                {
                    userManager.AddToRole(user.Id, "AlertTypes View");
                }

                if (!userManager.IsInRole(user.Id, "AlertTypes Create"))
                {
                    userManager.AddToRole(user.Id, "AlertTypes Create");
                }

                if (!userManager.IsInRole(user.Id, "AlertTypes Delete"))
                {
                    userManager.AddToRole(user.Id, "AlertTypes Delete");
                }

                if (!userManager.IsInRole(user.Id, "AlertTypes Edit"))
                {
                    userManager.AddToRole(user.Id, "AlertTypes Edit");
                }

                // Roles HistoricAlerts
                if (!userManager.IsInRole(user.Id, "HistoricAlerts"))
                {
                    userManager.AddToRole(user.Id, "HistoricAlerts");
                }

                if (!userManager.IsInRole(user.Id, "HistoricAlerts View"))
                {
                    userManager.AddToRole(user.Id, "HistoricAlerts View");
                }

                #endregion

                #region Products Group

                if (!userManager.IsInRole(user.Id, "Products Group"))
                {
                    userManager.AddToRole(user.Id, "Products Group");
                }

                // Roles ProductTypes
                if (!userManager.IsInRole(user.Id, "ProductTypes"))
                {
                    userManager.AddToRole(user.Id, "ProductTypes");
                }

                if (!userManager.IsInRole(user.Id, "ProductTypes View"))
                {
                    userManager.AddToRole(user.Id, "ProductTypes View");
                }

                if (!userManager.IsInRole(user.Id, "ProductTypes Create"))
                {
                    userManager.AddToRole(user.Id, "ProductTypes Create");
                }

                if (!userManager.IsInRole(user.Id, "ProductTypes Delete"))
                {
                    userManager.AddToRole(user.Id, "ProductTypes Delete");
                }

                if (!userManager.IsInRole(user.Id, "ProductTypes Edit"))
                {
                    userManager.AddToRole(user.Id, "ProductTypes Edit");
                }

                // Roles Products
                if (!userManager.IsInRole(user.Id, "Products"))
                {
                    userManager.AddToRole(user.Id, "Products");
                }

                if (!userManager.IsInRole(user.Id, "Products View"))
                {
                    userManager.AddToRole(user.Id, "Products View");
                }

                if (!userManager.IsInRole(user.Id, "Products Create"))
                {
                    userManager.AddToRole(user.Id, "Products Create");
                }

                if (!userManager.IsInRole(user.Id, "Products Delete"))
                {
                    userManager.AddToRole(user.Id, "Products Delete");
                }

                if (!userManager.IsInRole(user.Id, "Products Edit"))
                {
                    userManager.AddToRole(user.Id, "Products Edit");
                }

                #endregion

                #region Users Group

                // Roles Users
                if (!userManager.IsInRole(user.Id, "Users"))
                {
                    userManager.AddToRole(user.Id, "Users");
                }

                if (!userManager.IsInRole(user.Id, "Users View"))
                {
                    userManager.AddToRole(user.Id, "Users View");
                }

                if (!userManager.IsInRole(user.Id, "Users Create"))
                {
                    userManager.AddToRole(user.Id, "Users Create");
                }

                if (!userManager.IsInRole(user.Id, "Users Edit"))
                {
                    userManager.AddToRole(user.Id, "Users Edit");
                }

                if (!userManager.IsInRole(user.Id, "Users Delete"))
                {
                    userManager.AddToRole(user.Id, "Users Delete");
                }

                #endregion

                #region Sells Group

                // Roles Sells
                if (!userManager.IsInRole(user.Id, "Sells"))
                {
                    userManager.AddToRole(user.Id, "Sells");
                }

                if (!userManager.IsInRole(user.Id, "Sells View"))
                {
                    userManager.AddToRole(user.Id, "Sells View");
                }

                if (!userManager.IsInRole(user.Id, "Sells Create"))
                {
                    userManager.AddToRole(user.Id, "Sells Create");
                }

                if (!userManager.IsInRole(user.Id, "Sells Delete"))
                {
                    userManager.AddToRole(user.Id, "Sells Delete");
                }

                if (!userManager.IsInRole(user.Id, "Sells Edit"))
                {
                    userManager.AddToRole(user.Id, "Sells Edit");
                }

                #endregion

                #region General Group

                // Role Admin geral
                if (!userManager.IsInRole(user.Id, "Admin"))
                {
                    userManager.AddToRole(user.Id, "Admin");
                }

                #endregion

            }


        }

    }
}