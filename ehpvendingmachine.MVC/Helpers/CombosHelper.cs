﻿using Microsoft.Ajax.Utilities;

namespace ehpvendingmachine.MVC.Helpers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using ehpvendingmachine.ClassLibrary.Models;
    using ehpvendingmachine.MVC.Models;
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;

    public class CombosHelper : IDisposable
    {
        private static DataContextLocal db = new DataContextLocal();
        private static ApplicationDbContext dc = new ApplicationDbContext();

        /// <summary>
        /// Lista de tipos de pordutos para DorpDownList
        /// </summary>
        /// <returns></returns>
        public static List<ProductType> GetProductTypes()
        {
            var productTypes = db.ProductTypes.ToList();
            productTypes.Add(new ProductType
            {
                ProductTypeId = 0,
                Segment = "[Select a product type]"
            });

            return productTypes.OrderBy(d => d.Segment).ToList();
        }

        /// <summary>
        /// Lista de moedas para DorpDownList
        /// </summary>
        /// <returns></returns>
        public static List<CurrencyValue> GetCurrencyValuesString()
        {
            var currenctValueList = db.CurrencyValues.ToList();

            List<CurrencyValue> currenctValueListString = new List<CurrencyValue>();

            currenctValueListString.Add(new CurrencyValue
            {
                CurrencyValueId = 0,
                CurrencyName = "[Select a currency]"
            });

            foreach (var currencyValue in currenctValueList)
            {
                currenctValueListString.Add(new CurrencyValue
                {
                    CurrencyValueId = currencyValue.CurrencyValueId,
                    CurrencyName = currencyValue.Currency.ToString("C2")
                });
            }

            return currenctValueListString.OrderBy(c => c.CurrencyName).ToList();
        }

        /// <summary>
        /// Lista de capacidades para DorpDownList
        /// </summary>
        /// <returns></returns>
        public static List<Capacity> GetCapacitiesString()
        {
            var capacitiesList = db.Capacities.ToList();

            List<Capacity> capacitiesListString = new List<Capacity>();

            capacitiesListString.Add(new Capacity
            {
                CapacityId = 0,
                Name = "[Select a Capacity]"
            });

            foreach (var capacities in capacitiesList)
            {
                capacitiesListString.Add(new Capacity
                {
                    CapacityId = capacities.CapacityId,
                    Name = $"{capacities.Name} ({capacities.Quantity} Un)"
                });
            }

            return capacitiesListString.OrderBy(c => c.Name).ToList();
        }

        /// <summary>
        /// Lista de tipos de alertas numa dropdownlist
        /// </summary>
        /// <returns></returns>
        public static List<AlertType> GetAlertTypeList()
        {
            var alertTypesList = db.AlertTypes.ToList();

            alertTypesList.Add(new AlertType
            {
                AlertTypeId= 0,
                Name = "[Select a Name]"
            });

            return alertTypesList.OrderBy(s => s.Name).ToList();
        }

        /// <summary>
        /// Lista de definiçoes de alertas numa dropdownlist
        /// </summary>
        /// <returns></returns>
        public static List<SetAlert> GetSetAlertList()
        {
            var setAlertsList = db.SetAlerts.ToList();

            setAlertsList.Add(new SetAlert
            {
                AlertTypeId = 0,
                Description = "[Select a Description]"
            });

            return setAlertsList.OrderBy(s => s.Description).ToList();
        }

        /// <summary>
        /// dropdownlist do enum usertype
        /// </summary>
        /// <returns></returns>
        public static List<string> ListUserType()
        {

            List<string> UserTypes = new List<string>();

            UserTypes = Enum.GetValues(typeof(UserType))
                        .Cast<UserType>()
                        .Select(v => v.ToString())
                        .ToList();


            UserTypes.Add("[Select User Type]");

            //nao mostra os clientes
            UserTypes.Remove(UserType.Client.ToString());

            UserTypes.Sort();

            return UserTypes;
        }

        /// <summary>
        /// Lista de utilizadores
        /// </summary>
        /// <returns></returns>
        public static List<UserModel> ListUsers(UserManager<ApplicationUser> userManager)
        {
            //var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(dc));

            // Lista de Users existentes
            var users = userManager.Users.ToList();

            // Lista do tipo UserView
            var usersView = new List<UserModel>();

            // Passar para a View
            foreach (var user in users)
            {
                //mostra todos os que nao forem o superadmin
                if (user.Email != "sa@ehpvendingmachine.com")
                {
                    var userView = new UserModel
                    {
                        Email = user.Email,

                        UserName = user.UserName,
                        UserId = user.Id,
                        UserType = user.UserType,
                        PhoneNumber = user.PhoneNumber,
                        Adress = user.Adress,
                        Location = user.Location,
                        FirstName = user.FirstName,
                        LastName = user.LastName,
                        TIN = user.TIN
                    };

                    usersView.Add(userView);
                }

            }

            return usersView;
        }

        /// <summary>
        /// Lista de produtos para DorpDownList
        /// </summary>
        /// <returns></returns>
        public static List<Product> GetProductsList()
        {
            var productsList = db.Products.ToList();

            productsList.Add(new Product
            {
                ProductId = 0,
                Name = "[Select a product]",
            });

            return productsList.OrderBy(p => p.ProductId).ToList();
        }


        /// <summary>
        /// Lista de máquina para DorpDownList
        /// </summary>
        /// <returns></returns>
        public static List<Machine> GetMachinesList()
        {
            var machineList = db.Machines.ToList();

            List<Machine> newMachineList = new List<Machine>();

            foreach (var machine in machineList)
            {
                newMachineList.Add(new Machine
                {
                    MachineId = machine.MachineId,
                    MachineIdString = $"{machine.MachineId}"
                });
            }

            newMachineList.Add(new Machine
            {
                MachineId = 0,
                MachineIdString = "[Select a machine]",
            });

            return newMachineList.OrderBy(p => p.MachineId).ToList();
        }


        /// <summary>
        /// lista dos utilizadores com maquinas por atribuir mas com pedido feito
        /// </summary>
        /// <returns></returns>
        public static List<UserModel> ListUsersRequest()
        {
            //pedidos nao respondidos
            var userRequest = db.RequestMachines.Where(x => x.Assigned == false).ToList();

            var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(dc));

            // Lista de Users existentes
            var users = userManager.Users.Where(x => x.UserType == UserType.Client.ToString()).ToList();

            //lista de pedidos
            var requestmachine = userRequest.Select(product => product.UserId).ToList();

            //lista de pedidos iguais as maq atribuidas aos users
            IEnumerable<ApplicationUser> except = users.Where(product => requestmachine.Contains(product.Id));

            // Lista do tipo UserView
            var usersView = new List<UserModel>();

            foreach (var request in except)
            {
                var userView = new UserModel
                {
                    Email = request.Email,
                    UserName = request.UserName,
                    UserId = request.Id,
                };

                usersView.Add(userView);

            }

            usersView.Add(new UserModel
            {
                UserId = 0.ToString(),
                UserName = "[Select a User]"
            });

            return usersView.DistinctBy(x => x.UserId).OrderBy(u => u.UserId).ToList();
        }

        /// <summary>
        /// lista das maquinas por atribuir
        /// </summary>
        /// <returns></returns>
        public static List<Machine> ListMachineNotAssigned()
        {
            var assign = db.AssignMachines.ToList();

            var list = db.Machines.ToList();

            //lista de maquinas atribuidas
            var machineAssign = assign.Select(product => product.MachineId).ToList();

            //lista de maquinas nao atribuidas
            IEnumerable<Machine> except = list.Where(product => !machineAssign.Contains(product.MachineId));
            
            //os trocos
            var changesAssign = db.Changes.ToList();

            //quais as maquinas que tem trocos
            var changemachine = changesAssign.DistinctBy(x => x.MachineId).Select(product => product.MachineId).ToList();

            //maquinas nao atribuidas mas q ja tem atribuido produtos e trocos
            IEnumerable<Machine> exceptMachine = except.Where(product => changemachine.Contains(product.MachineId));


            //os produtos nas maquinas
            var productmachineAssign = db.ProductsMachines.ToList();

            //quais as maquinas que tem produtos
            var productmachine = productmachineAssign.DistinctBy(x => x.MachineId).Select(product => product.MachineId).ToList();

            //maquinas nao atribuidas mas q ja tem atribuido produtos e trocos
            IEnumerable<Machine> exceptProductMachine = exceptMachine.Where(product => productmachine.Contains(product.MachineId));

            var machineNotAssign = new List<Machine>();

            foreach (var item in exceptProductMachine)
            {
                string name = db.Capacities.Find(item.CapacityId).Name;

                //se nao estiver atribuido
                var machines = new Machine
                {
                    MachineId = item.MachineId,
                    MachineIdString = $"{item.MachineId} - ({name})"
                };
                machineNotAssign.Add(machines);
            }

            machineNotAssign.Add(new Machine
            {
                MachineId = 0,
                MachineIdString = "[Select a machine]",
            });

            return machineNotAssign.OrderBy(x => x.MachineId).ToList();
        }

        /// <summary>
        /// lista as maquinas atribuidas incluindo o username
        /// </summary>
        /// <returns></returns>
        public static List<AssignMachine> GetListAssignMachine()
        {
            var list = db.AssignMachines.ToList();

            var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(dc));

            // Lista de Users existentes
            var users = userManager.Users.Where(x => x.UserType == UserType.Client.ToString()).ToList();

            //lista de maquinas 
            var machineAssign = users.Select(x => x.Id).ToList();

            //lista de maquinas atribuidas
            IEnumerable<AssignMachine> except = list.Where(x => machineAssign.Contains(x.UserId));
            
            // Lista do tipo UserView
            var assignMachines = new List<AssignMachine>();
            
            // Passar para a View
            foreach (var item in except)
            {
                string name = dc.Users.Find(item.UserId).UserName;

                var assign = new AssignMachine
                {
                    AssignMachineId = item.AssignMachineId,
                    UserId = item.UserId,
                    MachineId = item.MachineId,
                    AssignDate = item.AssignDate,
                    UserName = name
                };
                assignMachines.Add(assign);
                
            }
            
            return assignMachines.ToList();
        }


        /// <summary>
        /// Fechar conecção à base de dados
        /// </summary>
        public void Dispose()
        {
            db.Dispose();
        }
    }
}