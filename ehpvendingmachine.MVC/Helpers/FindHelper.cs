﻿namespace ehpvendingmachine.MVC.Helpers
{
    using ehpvendingmachine.ClassLibrary.Models;
    using ehpvendingmachine.ClassLibrary.ViewModels;
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;

    public class FindHelper
    {
        private static ApplicationDbContext db = new ApplicationDbContext();

        /// <summary>
        /// encontra o utilizador
        /// </summary>
        /// <param name="userManager"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public static UserModel FindUser(UserManager<ApplicationUser> userManager, string id)
        {
            // Lista de Users existentes
            var user = userManager.Users.ToList().Find(u => u.Id == id);
            
            var userView = new UserModel
            {
                Email = user.Email,

                UserName = user.UserName,
                UserId = user.Id,
                UserType = user.UserType,
                PhoneNumber = user.PhoneNumber,
                Adress = user.Adress,
                Location = user.Location,
                FirstName = user.FirstName,
                LastName = user.LastName,
                TIN = user.TIN
            };

            return userView;
        }


        /// <summary>
        /// Retona uma lista do tipo SellsViewModel filtrada pelo campo search 
        /// </summary>
        /// <param name="sellsViewModelList"></param>
        /// <param name="search"></param>
        /// <returns></returns>
        public static List<SellsViewModel> GetListSellsViewModel(List<SellsViewModel> sellsViewModelList, string search)
        {
            if(string.IsNullOrEmpty(search))
            {
                return sellsViewModelList;
            }
            
            return sellsViewModelList.Where(s => s.Product.Name.ToUpper().Contains(search.ToUpper())).ToList();
        }


    }
}