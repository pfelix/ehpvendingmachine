﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ehpvendingmachine.MVC.Helpers
{
    public class ValidateHelper
    {
        /// <summary>
        /// Verifica se a quantidade é maior que a quantidade limite retornando verdade
        /// </summary>
        /// <param name="quantity"></param>
        /// <param name="quantityLimit"></param>
        /// <returns></returns>
        public static bool ValidateQuantity(int quantity, int quantityLimit)
        {
            if (quantity > quantityLimit)
                return true;

            return false;
        }

    }
}