﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web;

namespace ehpvendingmachine.MVC.Models
{
    using ClassLibrary.Models;

    [NotMapped]
    public class ProductsView : Product
    {
        [Display(Name = "Image")]
        public HttpPostedFileBase ImageFile { get; set; }
    }
}