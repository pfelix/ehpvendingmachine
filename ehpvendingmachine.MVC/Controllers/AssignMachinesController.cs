﻿namespace ehpvendingmachine.MVC.Controllers
{
    using ClassLibrary.Models;
    using Helpers;
    using Models;
    using System;
    using System.Data.Entity;
    using System.Linq;
    using System.Net;
    using System.Web.Mvc;

    [Authorize(Roles = "AssignMachines")]
    public class AssignMachinesController : Controller
    {
        private DataContextLocal db = new DataContextLocal();

        // GET: AssignMachines
        [Authorize(Roles = "AssignMachines View")]
        public ActionResult Index()
        {
            //var assignMachines = db.AssignMachines.Include(a => a.Machine);
           
            return View(CombosHelper.GetListAssignMachine());
        }
        
        // GET: AssignMachines/Create
        [Authorize(Roles = "AssignMachines Create")]
        public ActionResult Create()
        {
            ViewBag.UserId = new SelectList(CombosHelper.ListUsersRequest(), "UserId", "UserName");
            ViewBag.MachineId = new SelectList(CombosHelper.ListMachineNotAssigned(), "MachineId", "MachineIdString");
            return View();
        }

        // POST: AssignMachines/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "AssignMachineId,UserId,MachineId,AssignDate")] AssignMachine assignMachine)
        {
            // Verifica o user
            if (assignMachine.UserId == "0")
            {
                ViewBag.Error = "You have to select a user";

                ViewBag.UserId = new SelectList(CombosHelper.ListUsersRequest(), "UserId", "UserName");
                ViewBag.MachineId = new SelectList(CombosHelper.ListMachineNotAssigned(), "MachineId", "MachineIdString");
                return View(assignMachine);
            }

            // Verifica a maquina
            if (assignMachine.MachineId == 0)
            {
                ViewBag.Error = "You have to select a machine";

                ViewBag.UserId = new SelectList(CombosHelper.ListUsersRequest(), "UserId", "UserName");
                ViewBag.MachineId = new SelectList(CombosHelper.ListMachineNotAssigned(), "MachineId", "MachineIdString");
                return View(assignMachine);
            }

            assignMachine.AssignDate = DateTime.Now;

            //altera o estado da atribuicao da requestmachine
            var assign = db.RequestMachines.FirstOrDefault(x => x.UserId == assignMachine.UserId);
            
            assign.Assigned = true;

            if (ModelState.IsValid)
            {
                try
                {
                    db.AssignMachines.Add(assignMachine);
                    db.SaveChanges();

                    //grava as mudancas no requestmachine
                    db.Entry(assign).State = EntityState.Modified;
                    db.SaveChanges();
                   

                    return RedirectToAction("Index");
                }
                catch
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }


            }
            ViewBag.UserId = new SelectList(CombosHelper.ListUsersRequest(), "UserId", "UserName");
            ViewBag.MachineId = new SelectList(CombosHelper.ListMachineNotAssigned(), "MachineId", "MachineIdString", assignMachine.MachineId);
            return View(assignMachine);
        }
        
        // GET: AssignMachines/Delete/5
        [Authorize(Roles = "AssignMachines Delete")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            AssignMachine assignMachine = db.AssignMachines.Find(id);
            if (assignMachine == null)
            {
                return HttpNotFound();
            }
            return View(assignMachine);
        }

        // POST: AssignMachines/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            AssignMachine assignMachine = db.AssignMachines.Find(id);

            //altera o estado da atribuicao da requestmachine
            var assign = db.RequestMachines.FirstOrDefault(x => x.UserId == assignMachine.UserId);
            
            assign.Assigned = false;

            try
            {
                db.AssignMachines.Remove(assignMachine);
                db.SaveChanges();

                //grava as mudancas no requestmachine
                db.Entry(assign).State = EntityState.Modified;
                db.SaveChanges();

                return RedirectToAction("Index");
            }
            catch
            {
                return HttpNotFound();
            }
            
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
