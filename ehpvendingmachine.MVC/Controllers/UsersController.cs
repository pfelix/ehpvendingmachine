﻿namespace ehpvendingmachine.MVC.Controllers
{
    using ehpvendingmachine.ClassLibrary.Models;
    using ehpvendingmachine.MVC.Helpers;
    using ehpvendingmachine.MVC.Models;
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using System.Net;
    using System.Web.Mvc;

    [Authorize(Roles = "Users, Client")]
    public class UsersController : Controller
    {
        private DataContextLocal db = new DataContextLocal(); 
        private ApplicationDbContext dba = new ApplicationDbContext();

        // GET: Users
        [Authorize(Roles = "Users View")]
        public ActionResult Index()
        {
            var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(dba));
            return View(CombosHelper.ListUsers(userManager));
        }

        // GET: Users/Details/5
        [Authorize(Roles = "Users View")]
        public ActionResult Details(string userId)
        {
            var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(dba));

            if (string.IsNullOrEmpty(userId))
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            if (FindHelper.FindUser(userManager, userId) == null)
            {
                return HttpNotFound();
            }
            
            return View(FindHelper.FindUser(userManager, userId));
           
        }

        // GET: Products/Edit/5
        [Authorize(Roles = "Users Edit")]
        public ActionResult Edit(string userId)
        {
            var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(dba));

            if (string.IsNullOrEmpty(userId))
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            
            if (FindHelper.FindUser(userManager, userId) == null)
            {
                return HttpNotFound();
            }

            return View(FindHelper.FindUser(userManager, userId));
            
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(UserModel model)
        {
            var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(dba));

            if (ModelState.IsValid)
            {
                try
                {
                    ApplicationUser user = userManager.FindById(model.UserId);

                    user.PhoneNumber = model.PhoneNumber;
                    user.Adress = model.Adress;
                    user.Location = model.Location;
                    user.FirstName = model.FirstName;
                    user.LastName = model.LastName;
                    user.TIN = model.TIN;

                    userManager.Update(user);

                    if (user.UserType == "Client")
                    {
                        return RedirectToAction("Index", "Manage");
                    }

                    return RedirectToAction("Index");
                }
                catch
                {
                    return HttpNotFound();
                }
               
            }
            return View(model);
        }

        // GET: Users/Delete/5
        [Authorize(Roles = "Users Delete")]
        public ActionResult Delete(string userId)
        {
            var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(dba));

            if (string.IsNullOrEmpty(userId))
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            UserModel user = FindHelper.FindUser(userManager, userId);

            if (user == null)
            {
                return HttpNotFound();
            }

            // Verificar se user tem máquina associadas
            var userWithMachine = db.AssignMachines.FirstOrDefault(u => u.UserId == user.UserId);

            if (userWithMachine != null)
            {
                ViewBag.Error = "The user have machines associate and can't be deleted.";

                return View("Index",CombosHelper.ListUsers(userManager));
            }

            return View(user);
        }

        // POST: Products/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string userId)
        {
            var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(dba));
            var user = userManager.Users.ToList().Find(u => u.Id == userId);

            RequestMachine request = db.RequestMachines.FirstOrDefault(x => x.UserId == userId);

            if (string.IsNullOrEmpty(userId))
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            if (FindHelper.FindUser(userManager, userId) == null)
            {
                return HttpNotFound();
            }

            try
            {

                userManager.Delete(user);

                //caso exista um pedido, apaga o registo
                if (request != null)
                {
                    db.RequestMachines.Remove(request);
                    db.SaveChanges();
                }
            }
            catch
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }


            return View("Index", CombosHelper.ListUsers(userManager));

        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                dba.Dispose();
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
