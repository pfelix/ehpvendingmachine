﻿namespace ehpvendingmachine.MVC.Controllers
{
    using ClassLibrary.Models;
    using System.Linq;
    using System.Web.Mvc;

    [Authorize(Roles = "Admin, User")]
    public class DashBoardsController : Controller
    {
        private DataContext db = new DataContext();

        // GET: DashBoards
        public ActionResult Index()
        {
            var modelProductCounted = db.Products.ToList();
            int countProduct = modelProductCounted.Count();

            var modelMachineCounted = db.Machines.ToList();
            int countMachine = modelMachineCounted.Count();

            var modelAssigMacCounted = db.AssignMachines.ToList();
            int countAMachine = modelAssigMacCounted.Count();

            var modelSellCounted = db.Sells.ToList();
            int countSell = modelSellCounted.Count();

            var modelAlertCounted = db.AlertTypes.ToList();
            int countAlert = modelAlertCounted.Count();

            var modelHistoricCounted = db.HistoricAlerts.ToList();
            int countHistoric = modelHistoricCounted.Count();

            var modelMachineRequestedCounted = db.RequestMachines.Where(x => x.Assigned == false).ToList();
            int countMachineRequested = modelMachineRequestedCounted.Count();

            var modelProductMachineCounted = db.ProductsMachines.GroupBy(p => p.ProductId);
            int countProductMachine = modelProductMachineCounted.Count();

            ViewBag.Message = "ehp Vending Machine Dashboard!";

            DashBoard myDashboard = new DashBoard
            {
                ProductCounted = countProduct,
                MachineCounted = countMachine,
                AssignedMachineCounted = countAMachine,
                SellsCounted = countSell,
                AlertCounted = countAlert,
                HistorcAlertCounted = countHistoric,
                MachinesRequestedCounted = countMachineRequested,
                ProductMachinesCounted = countProductMachine
            };

            return View(myDashboard);
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
