﻿namespace ehpvendingmachine.MVC.Controllers
{
    using ClassLibrary.Models;
    using Models;
    using System;
    using System.Data.Entity;
    using System.Linq;
    using System.Net;
    using System.Web.Mvc;

    [Authorize(Roles = "ProductsMachine")]
    public class ProductsMachinesController : Controller
    {
        private DataContextLocal db = new DataContextLocal();

        // GET: ProductsMachines
        [Authorize(Roles = "ProductsMachine View")]
        public ActionResult Index(int? searchMachine)
        {
            ViewBag.searchMachine = new SelectList(Helpers.CombosHelper.GetMachinesList(), "MachineId", "MachineIdString", searchMachine);

            var productsMachines = db.ProductsMachines.Include(p => p.Machine).Include(p => p.Product);

            if (!(searchMachine == null || searchMachine == 0))
            {
                productsMachines = db.ProductsMachines.Where(p => p.MachineId == searchMachine).Include(p => p.Machine).Include(p => p.Product);
            }

            return View(productsMachines.ToList().OrderBy(p => p.MachineId));

        }

        // GET: ProductsMachines/Create
        [Authorize(Roles = "ProductsMachine Create")]
        public ActionResult Create()
        {
            ViewBag.MachineId = new SelectList(Helpers.CombosHelper.GetMachinesList(), "MachineId", "MachineIdString");
            ViewBag.ProductId = new SelectList(Helpers.CombosHelper.GetProductsList(), "ProductId", "Name");
            return View();
        }

        // POST: ProductsMachines/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ProductsMachineId,MachineId,ProductId,ProductQuantity")] ProductsMachine productsMachine)
        {
            // Verificar Produto selecionado
            if (productsMachine.ProductId == 0)
            {
                ViewBag.Error = "You have to select a product";

                ViewBag.MachineId = new SelectList(Helpers.CombosHelper.GetMachinesList(), "MachineId", "MachineIdString");
                ViewBag.ProductId = new SelectList(Helpers.CombosHelper.GetProductsList(), "ProductId", "Name");
                return View(productsMachine);
            }

            // Verificar Máquina selecionada
            if (productsMachine.MachineId == 0)
            {
                ViewBag.Error = "You have to select a machine";

                ViewBag.MachineId = new SelectList(Helpers.CombosHelper.GetMachinesList(), "MachineId", "MachineIdString");
                ViewBag.ProductId = new SelectList(Helpers.CombosHelper.GetProductsList(), "ProductId", "Name");
                return View(productsMachine);
            }

            // Verificar se produto existe
            Product product = db.Products.Find(productsMachine.ProductId);
            if(product == null)
            {
                ViewBag.Error = "The product does not exist, make sure the product is created.";

                ViewBag.MachineId = new SelectList(db.Machines, "MachineId", "MachineId", productsMachine.MachineId);
                ViewBag.ProductId = new SelectList(db.Products, "ProductId", "Name", productsMachine.ProductId);
                return View(productsMachine);
            }

            // Verificar se produto já existe na máquina
            var findProduct = db.ProductsMachines.FirstOrDefault(p => p.ProductId == productsMachine.ProductId && p.MachineId == productsMachine.MachineId);
            if (findProduct != null)
            {
                ViewBag.Error = "The product already exists on this machine";

                ViewBag.MachineId = new SelectList(Helpers.CombosHelper.GetMachinesList(), "MachineId", "MachineIdString");
                ViewBag.ProductId = new SelectList(Helpers.CombosHelper.GetProductsList(), "ProductId", "Name");
                return View(productsMachine);
            }

            // Verificar se máquina existe
            Machine machine = db.Machines.Find(productsMachine.MachineId);
            if (machine == null)
            {
                ViewBag.Error = "The machine does not exist, make sure the machine is created.";

                ViewBag.MachineId = new SelectList(db.Machines, "MachineId", "MachineId", productsMachine.MachineId);
                ViewBag.ProductId = new SelectList(db.Products, "ProductId", "Name", productsMachine.ProductId);
                return View(productsMachine);
            }

            // Preencher campo quantidade a 0
            productsMachine.ProductQuantity = 0;

            // Preencher campo validade
            int validityDays = db.Products.FirstOrDefault(p => p.ProductId == productsMachine.ProductId).ValidityDays;

            productsMachine.ValidityDate = DateTime.Now.AddDays(validityDays);

            if (ModelState.IsValid)
            {
                try
                {
                    db.ProductsMachines.Add(productsMachine);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                catch
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                
            }

            ViewBag.MachineId = new SelectList(Helpers.CombosHelper.GetMachinesList(), "MachineId", "MachineIdString");
            ViewBag.ProductId = new SelectList(Helpers.CombosHelper.GetProductsList(), "ProductId", "Name");
            return View(productsMachine);
        }

        // GET: ProductsMachines/Delete/5
        [Authorize(Roles = "ProductsMachine Delete")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ProductsMachine productsMachine = db.ProductsMachines.Find(id);
            if (productsMachine == null)
            {
                return HttpNotFound();
            }

            var productInSell = db.Sells.FirstOrDefault(p => p.ProductsMachineId == id);

            if (productInSell != null)
            {
                ViewBag.Error =
                    "The product can not be eliminated from the machine because there are sales associated with it.\nDisable the product so that it no longer appears on the machine";

                var productsMachines = db.ProductsMachines.Include(p => p.Machine).Include(p => p.Product);
                return View("Index", productsMachines.ToList());
            }

            return View(productsMachine);
        }

        // POST: ProductsMachines/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ProductsMachine productsMachine = db.ProductsMachines.Find(id);

            if (productsMachine == null)
            {
                return HttpNotFound();
            }

            try
            {
                db.ProductsMachines.Remove(productsMachine);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            catch
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
