﻿namespace ehpvendingmachine.MVC.Controllers
{
    using ClassLibrary.Models;
    using Models;
    using System.Data.Entity;
    using System.Linq;
    using System.Net;
    using System.Threading.Tasks;
    using System.Web.Mvc;

    [Authorize(Roles = "CurrencyValues")]
    public class CurrencyValuesController : Controller
    {
        private DataContextLocal db = new DataContextLocal();

        // GET: CurrencyValues
        [Authorize(Roles = "CurrencyValues View")]
        public async Task<ActionResult> Index()
        {
            return View(await db.CurrencyValues.OrderBy(c => c.Currency).ToListAsync());
        }

        // GET: CurrencyValues/Create
        [Authorize(Roles = "CurrencyValues Create")]
        public ActionResult Create()
        {
            return View();
        }

        // POST: CurrencyValues/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "CurrencyValueId,Currency")] CurrencyValue currencyValue)
        {
            // Verificar se moeda já existe
            var findCoin = db.CurrencyValues.FirstOrDefault(c => (double)c.Currency == (double)currencyValue.Currency);

            if (findCoin != null)
            {
                ViewBag.Error = "There can't be two coins of the same value";

                return View(currencyValue);
            }

            if (ModelState.IsValid)
            {
                try
                {
                    db.CurrencyValues.Add(currencyValue);
                    await db.SaveChangesAsync();
                    return RedirectToAction("Index");
                }
                catch{
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
            }

            return View(currencyValue);
        }

        // GET: CurrencyValues/Delete/5
        [Authorize(Roles = "CurrencyValues Delete")]
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CurrencyValue currencyValue = await db.CurrencyValues.FindAsync(id);
            if (currencyValue == null)
            {
                return HttpNotFound();
            }

            // Verificar se moeda já está como trocos em alguma máquina
            var coinInChanges = db.Changes.FirstOrDefault(c => c.CurrencyValueId == id);

            if (coinInChanges != null)
            {
                ViewBag.Error = "Currency can't be deleted because it's defined in the changes";

                return View("Index", await db.CurrencyValues.OrderBy(c => c.Currency).ToListAsync());
            }

            return View(currencyValue);
        }

        // POST: CurrencyValues/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            try
            {
                CurrencyValue currencyValue = await db.CurrencyValues.FindAsync(id);
                db.CurrencyValues.Remove(currencyValue);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            catch
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
