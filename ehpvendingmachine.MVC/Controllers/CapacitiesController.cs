﻿namespace ehpvendingmachine.MVC.Controllers
{
    using ClassLibrary.Models;
    using Models;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using System.Net;
    using System.Web.Mvc;

    [Authorize(Roles = "Capacities")]
    public class CapacitiesController : Controller
    {
        private DataContextLocal db = new DataContextLocal();

        // GET: Capacities
        [Authorize(Roles = "Capacities View")]
        public ActionResult Index()
        {
            return View(db.Capacities.ToList());
        }

        // GET: Capacities/Create
        [Authorize(Roles = "Capacities Create")]
        public ActionResult Create()
        {
            return View();
        }

        // POST: Capacities/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "CapacityId,Name,Quantity")] Capacity capacity)
        {
            // Verificar se existe um registo com a mesma capacity
            var findCapacity = db.Capacities.FirstOrDefault(c => c.Quantity == capacity.Quantity);

            if (findCapacity != null)
            {
                ViewBag.Error = "A record with this capability already exists";

                return View(capacity);
            }

            if (ModelState.IsValid)
            {
                try
                {
                    db.Capacities.Add(capacity);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                catch
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
            }

            return View(capacity);
        }

        // GET: Capacities/Edit/5
        [Authorize(Roles = "Capacities Edit")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Capacity capacity = db.Capacities.Find(id);
            if (capacity == null)
            {
                return HttpNotFound();
            }
            return View(capacity);
        }

        // POST: Capacities/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "CapacityId,Name,Quantity")] Capacity capacity)
        {
            // Verificar se existe um registo com a mesma capacity
            var findCapacity = db.Capacities.FirstOrDefault(c => c.Quantity == capacity.Quantity && c.CapacityId != capacity.CapacityId);

            if (findCapacity != null)
            {
                ViewBag.Error = "A record with this capability already exists";

                return View(capacity);
            }

            // verificar se nova capacidade pode ser utilizada, devido às quantidades já existentes

            List<ProductsMachine> productsMachine = db.ProductsMachines.ToList();
            List<Machine> machines = db.Machines.Where(m => m.CapacityId == capacity.CapacityId).ToList();

            List<ProductsMachine> prodctsWithMachines = (from productsMachines in productsMachine
                                                         join machine in machines on productsMachines.MachineId equals machine.MachineId
                                                         select new ProductsMachine{ MachineId = productsMachines.MachineId, ProductQuantity = productsMachines.ProductQuantity }).ToList();

            List<ProductsMachine> sumProdctsInMachines = prodctsWithMachines
                                                        .GroupBy(m => m.MachineId)
                                                        .Select(p => new ProductsMachine
                                                        {
                                                            MachineId = p.First().MachineId,
                                                            ProductQuantity = p.Sum(c => c.ProductQuantity)
                                                        }).ToList();

            var checkQuantityInProducts = sumProdctsInMachines.Where(p => p.ProductQuantity > capacity.Quantity).ToList();

            if (checkQuantityInProducts.Count != 0)
            {
                ViewBag.Error = "You can not change the quantity because there are machines with more products";
                return View(capacity);
            }


            if (ModelState.IsValid)
            {
                try
                {
                    db.Entry(capacity).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                catch
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
            }
            return View(capacity);
        }

        // GET: Capacities/Delete/5
        [Authorize(Roles = "Capacities Delete")]
        public ActionResult Delete(int? id)
        {
            // Verificar se capatidade já existe numa máquina
            var findCapacity = db.Machines.FirstOrDefault(c => c.CapacityId == id);
            if (findCapacity != null)
            {
                ViewBag.Error = "A record with this capability already exists";

                return View("Index", db.Capacities.ToList());
            }

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Capacity capacity = db.Capacities.Find(id);
            if (capacity == null)
            {
                return HttpNotFound();
            }
            return View(capacity);
        }

        // POST: Capacities/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Capacity capacity = db.Capacities.Find(id);
            try
            {
                db.Capacities.Remove(capacity);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            catch
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
