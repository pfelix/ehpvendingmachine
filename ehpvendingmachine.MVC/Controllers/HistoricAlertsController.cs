﻿using System;
using System.Collections.Generic;
using ehpvendingmachine.ClassLibrary.ViewModels;

namespace ehpvendingmachine.MVC.Controllers
{
    using ClassLibrary.Models;
    using System.Data.Entity;
    using System.Linq;
    using System.Net;
    using System.Web.Mvc;

    [Authorize(Roles = "HistoricAlerts")]
    public class HistoricAlertsController : Controller
    {
        private DataContext db = new DataContext();

        // GET: HistoricAlerts
        [Authorize(Roles = "HistoricAlerts View")]
        public ActionResult Index(int? searchMachine)
        {

            ViewBag.searchMachine = new SelectList(Helpers.CombosHelper.GetMachinesList(), "MachineId", "MachineIdString", searchMachine);

            List<HistoricAlertsViewModel> historicAlertsViewModel = new List<HistoricAlertsViewModel>();

            var historicList = db.HistoricAlerts.Include(h => h.Machine).Include(h => h.SetAlert).ToList();

            if (!(searchMachine == null || searchMachine == 0))
            {
                historicList = db.HistoricAlerts.Where(m => m.MachineId == searchMachine).Include(h => h.Machine).Include(h => h.SetAlert).ToList();
            }

            foreach (var historic in historicList)
            {
                HistoricAlertsViewModel historicAlertsView = new HistoricAlertsViewModel();

                historicAlertsView.HistoricAlert = historic;
                historicAlertsView.SetAlert = db.SetAlerts.FirstOrDefault(s => s.SetAlertId == historic.SetAlertId);
                historicAlertsView.Priority = db.Priorities.FirstOrDefault(s => s.PriorityId == historicAlertsView.SetAlert.PriorityId);
                historicAlertsView.AlertType = db.AlertTypes.FirstOrDefault(s => s.AlertTypeId == historicAlertsView.SetAlert.AlertTypeId);

                historicAlertsViewModel.Add(historicAlertsView);
            }

            return View(historicAlertsViewModel);
        }
        
        //dá o alerta como tratado
        [Authorize(Roles = "HistoricAlerts Edit")]
        public ActionResult Resolution(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            HistoricAlert historicAlert = db.HistoricAlerts.Find(id);
            if (historicAlert == null)
            {
                return HttpNotFound();
            }

            //mete a data de hj
            historicAlert.ResolutionDate = DateTime.Now;

            if (ModelState.IsValid)
            {
                try
                {
                    db.Entry(historicAlert).State = EntityState.Modified;
                    db.SaveChanges();

                    return RedirectToAction("Index");
                }
                catch
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                
            }

            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
