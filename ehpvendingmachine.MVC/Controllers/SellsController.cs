﻿namespace ehpvendingmachine.MVC.Controllers
{
    using ClassLibrary.Models;
    using ehpvendingmachine.ClassLibrary.ViewModels;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using System.Web.Mvc;

    [Authorize(Roles = "Sells")]
    public class SellsController : Controller
    {
        private DataContext db = new DataContext();

        // GET: Sells
        [Authorize(Roles = "Sells View")]
        public ActionResult Index(string search, int? searchMachine)
        {
            ViewBag.searchMachine = new SelectList(Helpers.CombosHelper.GetMachinesList(), "MachineId", "MachineIdString", searchMachine);

            List<SellsViewModel> sellsViewModelList = new List<SellsViewModel>();
            
            var sells = db.Sells.Include(s => s.ProductsMachine).ToList();

            

            foreach (var item in sells)
            {
                SellsViewModel sellsViewModel = new SellsViewModel();

                int machineId = db.ProductsMachines.Find(item.ProductsMachineId).MachineId;

                if (!(searchMachine == null || searchMachine == 0))
                {
                    machineId = (int)searchMachine;
                }
                
                int productId = db.ProductsMachines.Find(item.ProductsMachineId).ProductId;

                sellsViewModel.Sell = db.Sells.Find(item.SellId);
                sellsViewModel.Product = db.Products.Find(productId);
                sellsViewModel.Machine = db.Machines.Find(machineId);
                sellsViewModel.Value = sellsViewModel.Product.Price * sellsViewModel.Sell.Quantity;

                sellsViewModelList.Add(sellsViewModel);
            }

            var list = Helpers.FindHelper.GetListSellsViewModel(sellsViewModelList, search);

            return View(list);
        }
   
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
