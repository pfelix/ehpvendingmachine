﻿using System.Collections.Generic;

namespace ehpvendingmachine.MVC.Controllers
{
    using ClassLibrary.Models;
    using Models;
    using System;
    using System.Data.Entity;
    using System.Linq;
    using System.Net;
    using System.Web.Mvc;

    [Authorize(Roles = "Machines")]
    public class MachinesController : Controller
    {
        private DataContextLocal db = new DataContextLocal();

        // GET: Machines
        [Authorize(Roles = "Machines View")]
        public ActionResult Index(int? searchMachine, int? searchCapacity)
        {
            var machines = LoadDropDownListAndReturnMachines(searchMachine, searchCapacity);

            return View(machines.OrderBy(m => m.MachineId));
        }

        // GET: Machines/Create
        [Authorize(Roles = "Machines Create")]
        public ActionResult Create()
        {
            ViewBag.CapacityId = new SelectList(Helpers.CombosHelper.GetCapacitiesString(), "CapacityId", "Name");
            return View();
        }

        // POST: Machines/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "MachineId,CapacityId")] Machine machine)
        {
            // Verificar se selecioando capacidade
            if (machine.CapacityId == 0)
            {
                ViewBag.Error = "You have to select a capacity";

                ViewBag.CapacityId = new SelectList(Helpers.CombosHelper.GetCapacitiesString(), "CapacityId", "Name");

                return View(machine);
            }

            if (ModelState.IsValid)
            {
                try
                {
                    db.Machines.Add(machine);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                catch
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
            }

            ViewBag.CapacityId = new SelectList(Helpers.CombosHelper.GetCapacitiesString(), "CapacityId", "Name", machine.CapacityId);
            return View(machine);
        }

        // GET: Machines/Edit/5
        [Authorize(Roles = "Machines Edit")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Machine machine = db.Machines.Find(id);
            if (machine == null)
            {
                return HttpNotFound();
            }
            ViewBag.CapacityId = new SelectList(Helpers.CombosHelper.GetCapacitiesString(), "CapacityId", "Name", machine.CapacityId);
            return View(machine);
        }

        // POST: Machines/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "MachineId,CapacityId")] Machine machine)
        {
            // Verificar se selecioando capacidade
            if (machine.CapacityId == 0)
            {
                ViewBag.Error = "You have to select a capacity";

                ViewBag.CapacityId = new SelectList(Helpers.CombosHelper.GetCapacitiesString(), "CapacityId", "Name");

                return View(machine);
            }

            // Verificar se máquina tem mais produtos que a nova capacidade
            int newCapacity = db.Capacities.FirstOrDefault(c => c.CapacityId == machine.CapacityId).Quantity;

            int productsInMachine = db.ProductsMachines.Where(p => p.MachineId == machine.MachineId)
                .Sum(p => p.ProductQuantity);

            if (productsInMachine > newCapacity)
            {
                ViewBag.Error = $"You can not change to this capacity because there are {productsInMachine} products";

                ViewBag.CapacityId = new SelectList(Helpers.CombosHelper.GetCapacitiesString(), "CapacityId", "Name");

                return View(machine);
            }

            if (ModelState.IsValid)
            {
                try
                {
                    db.Entry(machine).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                catch
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
            }

            ViewBag.CapacityId = new SelectList(Helpers.CombosHelper.GetCapacitiesString(), "CapacityId", "Name", machine.CapacityId);
            return View(machine);
        }

        // GET: Machines/Delete/5
        [Authorize(Roles = "Machines Delete")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Machine machine = db.Machines.Find(id);
            if (machine == null)
            {
                return HttpNotFound();
            }

            // Verificar se Maquina já tem produtos
            var findProduct = db.ProductsMachines.FirstOrDefault(p => p.MachineId == id);
            if (findProduct != null)
            {

                ViewBag.Error = "The machine already has associated products, so it can't be deleted.";

                var machines = LoadDropDownListAndReturnMachines(0,0);

                return View("Index", machines.OrderBy(m => m.MachineId));
            }

            return View(machine);
        }

        // POST: Machines/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            try
            {
                Machine machine = db.Machines.Find(id);
                db.Machines.Remove(machine);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            catch
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }


        /// <summary>
        /// Carrega as DropdownList searchMachine e searchCapacity.
        /// Retrona uma lista de máquinas
        /// </summary>
        /// <param name="searchMachine"></param>
        /// <param name="searchCapacity"></param>
        /// <returns></returns>
        private List<Machine> LoadDropDownListAndReturnMachines(int? searchMachine, int? searchCapacity)
        {
            var machines = db.Machines.Include(m => m.Capacity);

            ViewBag.searchMachine = new SelectList(Helpers.CombosHelper.GetMachinesList(), "MachineId", "MachineIdString", searchMachine);
            ViewBag.searchCapacity = new SelectList(Helpers.CombosHelper.GetCapacitiesString(), "CapacityId", "Name", searchCapacity);

            if (!(searchMachine == null || searchMachine == 0))
            {
                machines = db.Machines.Where(m => m.MachineId == searchMachine).Include(m => m.Capacity);
            }

            if (!(searchCapacity == null || searchCapacity == 0))
            {
                machines = db.Machines.Where(m => m.CapacityId == searchCapacity).Include(m => m.Capacity);
            }

            return machines.ToList();
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
