﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ehpvendingmachine.ClassLibrary.Models;
using ehpvendingmachine.MVC.Models;
using Microsoft.AspNet.Identity;

namespace ehpvendingmachine.MVC.Controllers
{
    public class RequestMachinesController : Controller
    {
        private DataContextLocal db = new DataContextLocal();
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Request()
        {
            RequestMachine requestMachine = new RequestMachine
            {
                UserId = User.Identity.GetUserId(),
                Assigned = false
            };
            
            if (ModelState.IsValid)
            {
                try
                {
                    db.RequestMachines.Add(requestMachine);
                    db.SaveChanges();
                    return RedirectToAction("Index", "Manage");
                    
                }
                catch
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }

            }

            return View(requestMachine);
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
