﻿namespace ehpvendingmachine.MVC.Controllers
{
    using ClassLibrary.Models;
    using Models;
    using System.Data.Entity;
    using System.Linq;
    using System.Net;
    using System.Web.Mvc;

    [Authorize(Roles = "Changes")]
    public class ChangesController : Controller
    {
        private DataContextLocal db = new DataContextLocal();

        // GET: Changes
        [Authorize(Roles = "Changes View")]
        public ActionResult Index(int? searchMachine)
        {
            // Carregar DropDownList com todas as maquina
            ViewBag.searchMachine = new SelectList(Helpers.CombosHelper.GetMachinesList(), "MachineId", "MachineIdString", searchMachine);

            var changes = db.Changes.Include(c => c.CurrencyValue).Include(c => c.Machine);

            if (!(searchMachine == null || searchMachine == 0))
            {
                changes = db.Changes.Where(c => c.MachineId == searchMachine).Include(c => c.CurrencyValue).Include(c => c.Machine);
            }
            
            return View(changes.ToList());
        }

        // GET: Changes/Create
        [Authorize(Roles = "Changes Create")]
        public ActionResult Create()
        {
            ViewBag.CurrencyValueId = new SelectList(Helpers.CombosHelper.GetCurrencyValuesString(), "CurrencyValueId", "CurrencyName");
            ViewBag.MachineId = new SelectList(Helpers.CombosHelper.GetMachinesList(), "MachineId", "MachineIdString");
            return View();
        }

        // POST: Changes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ChangeId,CurrencyValueId,MachineId,Quantity,QuantityLimit")] Change change)
        {
            // Verificar se selecionado moeda
            if (change.CurrencyValueId == 0)
            {
                ViewBag.Error = "You have to select a currency";

                ViewBag.CurrencyValueId = new SelectList(Helpers.CombosHelper.GetCurrencyValuesString(), "CurrencyValueId", "CurrencyName", change.CurrencyValueId);
                ViewBag.MachineId = new SelectList(Helpers.CombosHelper.GetMachinesList(), "MachineId", "MachineIdString", change.MachineId);

                return View(change);
            }
            
            // Verificar se selecioando máquina
            if (change.MachineId == 0)
            {
                ViewBag.Error = "You have to select a Machine";

                ViewBag.CurrencyValueId = new SelectList(Helpers.CombosHelper.GetCurrencyValuesString(), "CurrencyValueId", "CurrencyName", change.CurrencyValueId);
                ViewBag.MachineId = new SelectList(Helpers.CombosHelper.GetMachinesList(), "MachineId", "MachineIdString", change.MachineId);

                return View(change);
            }

            // Verificar se quantidade é maior que quantidade limite
            if (Helpers.ValidateHelper.ValidateQuantity(change.Quantity, change.QuantityLimit))
            {
                ViewBag.Error = "The quantity must be equal to or less than the limit quantity.";

                ViewBag.CurrencyValueId = new SelectList(Helpers.CombosHelper.GetCurrencyValuesString(), "CurrencyValueId", "CurrencyName", change.CurrencyValueId);
                ViewBag.MachineId = new SelectList(Helpers.CombosHelper.GetMachinesList(), "MachineId", "MachineIdString", change.MachineId);

                return View(change);
            }

            // Verificar se moeda já existe na máquina
            var coinInMachine = db.Changes.FirstOrDefault(
                c => c.CurrencyValueId == change.CurrencyValueId &&
                     c.MachineId == change.MachineId);

            if (coinInMachine != null)
            {
                ViewBag.Error = "The currency already exists in the machine";

                ViewBag.CurrencyValueId = new SelectList(Helpers.CombosHelper.GetCurrencyValuesString(), "CurrencyValueId", "CurrencyName", change.CurrencyValueId);
                ViewBag.MachineId = new SelectList(Helpers.CombosHelper.GetMachinesList(), "MachineId", "MachineIdString", change.MachineId);

                return View(change);
            }

            // Garantir que campo Altered é falso
            change.Altered = false;
            change.Quantity = 0;

            if (ModelState.IsValid)
            {
                try
                {
                    db.Changes.Add(change);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                catch
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
            }

            ViewBag.CurrencyValueId = new SelectList(Helpers.CombosHelper.GetCurrencyValuesString(), "CurrencyValueId", "CurrencyName", change.CurrencyValueId);
            ViewBag.MachineId = new SelectList(Helpers.CombosHelper.GetMachinesList(), "MachineId", "MachineIdString", change.MachineId);
            return View(change);
        }

        // GET: Changes/Edit/5
        [Authorize(Roles = "Changes Edit")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Change change = db.Changes.Find(id);
            if (change == null)
            {
                return HttpNotFound();
            }

            ViewBag.CurrencyValueId = new SelectList(Helpers.CombosHelper.GetCurrencyValuesString(), "CurrencyValueId", "CurrencyName", change.CurrencyValueId);
            ViewBag.MachineId = new SelectList(Helpers.CombosHelper.GetMachinesList(), "MachineId", "MachineIdString", change.MachineId);
            return View(change);
        }

        // POST: Changes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ChangeId,CurrencyValueId,MachineId,Quantity,QuantityLimit")] Change change)
        {
            var findChange = db.Changes.AsNoTracking().FirstOrDefault(c => c.ChangeId == change.ChangeId);

            // Verificar se moeda foi apagada
            if(findChange == null)
            {
                return HttpNotFound();
            }

            // Verificar se selecionado moeda
            if (change.CurrencyValueId == 0)
            {
                ViewBag.Error = "You have to select a currency";

                ViewBag.CurrencyValueId = new SelectList(Helpers.CombosHelper.GetCurrencyValuesString(), "CurrencyValueId", "CurrencyName", change.CurrencyValueId);
                ViewBag.MachineId = new SelectList(Helpers.CombosHelper.GetMachinesList(), "MachineId", "MachineIdString", change.MachineId);

                return View(change);
            }

            // Verificar se selecioando máquina
            if (change.MachineId == 0)
            {
                ViewBag.Error = "You have to select a Machine";

                ViewBag.CurrencyValueId = new SelectList(Helpers.CombosHelper.GetCurrencyValuesString(), "CurrencyValueId", "CurrencyName", change.CurrencyValueId);
                ViewBag.MachineId = new SelectList(Helpers.CombosHelper.GetMachinesList(), "MachineId", "MachineIdString", change.MachineId);

                return View(change);
            }

            // Verificar se quantidade é maior que quantidade limite
            if (Helpers.ValidateHelper.ValidateQuantity(change.Quantity, change.QuantityLimit))
            {
                ViewBag.Error = "The quantity must be equal to or less than the limit quantity.";

                ViewBag.CurrencyValueId = new SelectList(Helpers.CombosHelper.GetCurrencyValuesString(), "CurrencyValueId", "CurrencyName", change.CurrencyValueId);
                ViewBag.MachineId = new SelectList(Helpers.CombosHelper.GetMachinesList(), "MachineId", "MachineIdString", change.MachineId);

                return View(change);
            }

            // Verificar se moeda já existe na máquina
            var coinInMachine = db.Changes.FirstOrDefault(
                c => c.CurrencyValueId == change.CurrencyValueId &&
                     c.MachineId == change.MachineId && c.ChangeId != change.ChangeId);

            if (coinInMachine != null)
            {
                ViewBag.Error = "The currency already exists in the machine";

                ViewBag.CurrencyValueId = new SelectList(Helpers.CombosHelper.GetCurrencyValuesString(), "CurrencyValueId", "CurrencyName", change.CurrencyValueId);
                ViewBag.MachineId = new SelectList(Helpers.CombosHelper.GetMachinesList(), "MachineId", "MachineIdString", change.MachineId);

                return View(change);
            }

            if (ModelState.IsValid)
            {
                try
                {
                    db.Entry(change).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                catch
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                
            }
            ViewBag.CurrencyValueId = new SelectList(Helpers.CombosHelper.GetCurrencyValuesString(), "CurrencyValueId", "CurrencyName", change.CurrencyValueId);
            ViewBag.MachineId = new SelectList(Helpers.CombosHelper.GetMachinesList(), "MachineId", "MachineIdString", change.MachineId);
            return View(change);
        }

        // GET: Changes/Delete/5
        [Authorize(Roles = "Changes Delete")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Change change = db.Changes.Find(id);
            if (change == null)
            {
                return HttpNotFound();
            }
            return View(change);
        }

        // POST: Changes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            try
            {
                Change change = db.Changes.Find(id);
                db.Changes.Remove(change);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            catch
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
