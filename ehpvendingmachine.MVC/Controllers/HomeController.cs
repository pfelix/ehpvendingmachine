﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ehpvendingmachine.MVC.Models;
using Microsoft.Ajax.Utilities;
using Microsoft.AspNet.Identity;

namespace ehpvendingmachine.MVC.Controllers
{
    public class HomeController : Controller
    {
        private DataContextLocal db = new DataContextLocal();

        public ActionResult Index()
        {
            var user = User.Identity.GetUserId();
            
            ViewBag.CountRequest = db.RequestMachines.Count(x => x.UserId == user);
            return View();
        }
        
        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}