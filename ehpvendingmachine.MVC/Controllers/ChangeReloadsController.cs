﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ehpvendingmachine.ClassLibrary.Models;
using ehpvendingmachine.MVC.Models;

namespace ehpvendingmachine.MVC.Controllers
{
    [Authorize(Roles = "ChangeReloads")]
    public class ChangeReloadsController : Controller
    {
        private DataContextLocal db = new DataContextLocal();

        // GET: Changes
        [Authorize(Roles = "ChangeReloads View")]
        public ActionResult Index(int? searchMachine)
        {
            // Carregar DropDownList com todas as maquina
            ViewBag.searchMachine = new SelectList(Helpers.CombosHelper.GetMachinesList(), "MachineId", "MachineIdString", searchMachine);

            var changes = db.Changes.Include(c => c.CurrencyValue).Include(c => c.Machine);

            if (!(searchMachine == null || searchMachine == 0))
            {
                changes = db.Changes.Where(c => c.MachineId == searchMachine).Include(c => c.CurrencyValue).Include(c => c.Machine);
            }

            return View(changes.ToList());
        }

        // GET: Changes/Edit/5
        [Authorize(Roles = "ChangeReloads Edit")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Change change = db.Changes.Find(id);
            if (change == null)
            {
                return HttpNotFound();
            }

            return View(change);
        }

        // POST: Changes/Delete/5
        [HttpPost, ActionName("Edit")]
        [ValidateAntiForgeryToken]
        public ActionResult EditConfirmed(int id)
        {
            Change change = db.Changes.Find(id);

            if (change == null)
            {
                return HttpNotFound();
            }

            int newChangeQuantity = change.QuantityLimit;

            change.Quantity = newChangeQuantity;
            change.Altered = true;

            try
            {
                db.Entry(change).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            catch
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}
