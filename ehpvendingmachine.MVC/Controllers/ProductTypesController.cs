﻿namespace ehpvendingmachine.MVC.Controllers
{
    using ClassLibrary.Models;
    using Models;
    using System;
    using System.Data.Entity;
    using System.Linq;
    using System.Net;
    using System.Web.Mvc;

    [Authorize(Roles = "ProductTypes")]
    public class ProductTypesController : Controller
    {
        private DataContextLocal db = new DataContextLocal();

        // GET: ProductTypes
        [Authorize(Roles = "ProductTypes View")]
        public ActionResult Index()
        {
            return View(db.ProductTypes.ToList());
        }


        // GET: ProductTypes/Create
        [Authorize(Roles = "ProductTypes Create")]
        public ActionResult Create()
        {
            return View();
        }

        // POST: ProductTypes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ProductTypeId,Segment")] ProductType productType)
        {
            var product = db.ProductTypes.FirstOrDefault(x => x.Segment.ToUpper() == productType.Segment.ToUpper());

            if (product != null)
            {
                ViewBag.Error = "A similar type already exists";
                return View();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    db.ProductTypes.Add(productType);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                catch
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }

            }

            return View(productType);
        }

        // GET: ProductTypes/Edit/5
        [Authorize(Roles = "ProductTypes Edit")]
        public ActionResult Edit(int? id)
        {
            var products = db.Products.FirstOrDefault(x => x.ProductTypeId == id);

            if (products != null)
            {
                ViewBag.Error = "Can not edit because there are products of this type.";

                return View("Index", db.ProductTypes.ToList());
            }
            
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ProductType productType = db.ProductTypes.Find(id);
            if (productType == null)
            {
                return HttpNotFound();
            }
            return View(productType);
        }

        // POST: ProductTypes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ProductTypeId,Segment")] ProductType productType)
        {
            var product = db.ProductTypes.FirstOrDefault(x => x.Segment.ToUpper() == productType.Segment.ToUpper());

            if (product != null)
            {
                ViewBag.Error = "A similar type already exists";
                return View();
            }
            
            if (ModelState.IsValid)
            {
                try
                {
                    db.Entry(productType).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                catch
                {
                    return HttpNotFound();
                }
                
            }
            return View(productType);
        }

        // GET: ProductTypes/Delete/5
        [Authorize(Roles = "ProductTypes Delete")]
        public ActionResult Delete(int? id)
        {
            var products = db.Products.FirstOrDefault(x => x.ProductTypeId == id);

            if (products != null)
            {
                ViewBag.Error = "Can not delete because there are products of this type.";

                return View("Index", db.ProductTypes.ToList());
            }


            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ProductType productType = db.ProductTypes.Find(id);
            if (productType == null)
            {
                return HttpNotFound();
            }
            return View(productType);
        }

        // POST: ProductTypes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ProductType productType = db.ProductTypes.Find(id);

            if (productType == null)
            {
                return HttpNotFound();
            }

            try
            {
                db.ProductTypes.Remove(productType);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            catch
            {
                return HttpNotFound();
            }
            
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
