﻿namespace ehpvendingmachine.MVC.Controllers
{
    using ClassLibrary.Models;
    using ClassLibrary.ViewModels;
    using Models;
    using System;
    using System.Data.Entity;
    using System.Linq;
    using System.Net;
    using System.Web.Mvc;

    [Authorize(Roles = "ProductsMachine Reload")]
    public class ProductReloadsController : Controller
    {

        DataContextLocal db = new DataContextLocal();

        // GET: ProductReloads
        [Authorize(Roles = "ProductsMachine Reload View")]
        public ActionResult Index(int? searchMachine)
        {
            ProductReloadViewModel productReloadViewModel = new ProductReloadViewModel();

            productReloadViewModel.ProductsMachines = db.ProductsMachines.OrderBy(p => p.MachineId).ToList();
            productReloadViewModel.Machines = db.Machines.ToList();

            // Carregar DropDownList com todas as maquina
            ViewBag.searchMachine = new SelectList(Helpers.CombosHelper.GetMachinesList(), "MachineId", "MachineIdString", searchMachine);

            if (!(searchMachine == null || searchMachine == 0))
            {
                productReloadViewModel.ProductsMachines = db.ProductsMachines.Where(m => m.MachineId == searchMachine).OrderBy(p => p.MachineId).ToList();
                productReloadViewModel.Machines = db.Machines.Where(m => m.MachineId == searchMachine).ToList();
            }

            return View(productReloadViewModel);

        }

        // GET: ProductReloads/AddAll
        [Authorize(Roles = "ProductsMachine Reload Add")]
        public ActionResult AddAll(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ProductsMachine productsMachine = db.ProductsMachines.Find(id);
            if (productsMachine == null)
            {
                return HttpNotFound();
            }

            return View(productsMachine);
        }

        // POST: ProductReloads/AddAll
        [HttpPost, ActionName("AddAll")]
        [ValidateAntiForgeryToken]
        public ActionResult AddAllConfirmed(int id)
        {
            try
            {
                ProductsMachine productsMachine = db.ProductsMachines.Find(id);

                // Quantidade atual de produtos na máquina
                int currentQuantity = db.ProductsMachines.Where(m => m.MachineId == productsMachine.MachineId)
                    .Sum(m => m.ProductQuantity);

                // Apanhar capacidade limite da máquina
                Machine machine = db.Machines.Find(productsMachine.MachineId);

                int capacityLimit = db.Capacities.Find(machine.CapacityId).Quantity;

                productsMachine.ProductQuantity = productsMachine.ProductQuantity + (capacityLimit - currentQuantity);

                // Aletra campo altered para true
                productsMachine.Altered = true;

                db.SaveChanges();

                return RedirectToAction("Index");
            }
            catch
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        // GET: ProductReloads/AddAll
        [Authorize(Roles = "ProductsMachine Reload Add")]
        public ActionResult AddxUn(int? id)
        {
            Session["productsMachine"] = null;

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ProductsMachine productsMachine = db.ProductsMachines.Find(id);
            if (productsMachine == null)
            {
                return HttpNotFound();
            }

            ViewBag.MachineId = new SelectList(Helpers.CombosHelper.GetMachinesList(), "MachineId", "MachineIdString", productsMachine.MachineId);
            ViewBag.ProductId = new SelectList(Helpers.CombosHelper.GetProductsList(), "ProductId", "Name", productsMachine.ProductId);
            return View(productsMachine);
        }

        // POST: ProductReloads/AddxUn
        [HttpPost, ActionName("AddxUn")]
        [ValidateAntiForgeryToken]
        public ActionResult AddxUn([Bind(Include = "ProductsMachineId,MachineId,ProductId,ProductQuantity")] ProductsMachine productsMachine)
        {
            // Apanhar quantidade limite
            Machine machine = db.Machines.Find(productsMachine.MachineId);

            if (machine == null)
            {
                return HttpNotFound();
            }

            int capacityLimit = db.Capacities.Find(machine.CapacityId).Quantity;

            // Apanhar quantidade existente
            int currentQuantityTotal = db.ProductsMachines.AsNoTracking().Where(m => m.MachineId == productsMachine.MachineId).ToList()
                .Sum(m => m.ProductQuantity);

            // Adicionar à quantidade existente o que vem do utilizador
            int newQuantity = productsMachine.ProductQuantity + currentQuantityTotal;

            // Verificar se ao adicionar os produtos passa o limite da máquina
            if (newQuantity > capacityLimit)
            {
                ViewBag.Error = $"You can only enter more {capacityLimit - currentQuantityTotal} Un";

                ViewBag.MachineId = new SelectList(Helpers.CombosHelper.GetMachinesList(), "MachineId", "MachineIdString", productsMachine.MachineId);
                ViewBag.ProductId = new SelectList(Helpers.CombosHelper.GetProductsList(), "ProductId", "Name", productsMachine.ProductId);
                return View(productsMachine);
            }

            // Apanhar a quantidade que já existe do produto na máquina
            int productQuantity = db.ProductsMachines.AsNoTracking().FirstOrDefault(p => p.ProductsMachineId ==productsMachine.ProductsMachineId).ProductQuantity;

            // Adicionar ao produto a quantidade do utilizador
            productsMachine.ProductQuantity = productsMachine.ProductQuantity + productQuantity;

            // Aletra campo altered para true
            productsMachine.Altered = true;

            // Preencher campo validade
            int validityDays = db.Products.FirstOrDefault(p => p.ProductId == productsMachine.ProductId).ValidityDays;
            productsMachine.ValidityDate = DateTime.Now.AddDays(validityDays);

            
            if (ModelState.IsValid)
            {
                try
                {
                    db.Entry(productsMachine).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                catch
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
            }

            ViewBag.MachineId = new SelectList(Helpers.CombosHelper.GetMachinesList(), "MachineId", "MachineIdString", productsMachine.MachineId);
            ViewBag.ProductId = new SelectList(Helpers.CombosHelper.GetProductsList(), "ProductId", "Name", productsMachine.ProductId);
            return View(productsMachine);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}