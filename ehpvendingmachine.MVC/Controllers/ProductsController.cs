﻿namespace ehpvendingmachine.MVC.Controllers
{
    using ClassLibrary.Models;
    using Helpers;
    using Models;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using System.Net;
    using System.Web.Mvc;

    [Authorize(Roles = "Products")]
    public class ProductsController : Controller
    {
        private DataContextLocal db = new DataContextLocal();

        // GET: Products
        [Authorize(Roles = "Products View")]
        public ActionResult Index(int? searchProductType, string searchName)
        {
            var products = LoadDropDownListAndReturnProducts(searchProductType, searchName);

            return View(products);
        }

        // GET: Products/Create
        [Authorize(Roles = "Products Create")]
        public ActionResult Create()
        {
            ViewBag.ProductTypeId = new SelectList(CombosHelper.GetProductTypes(), "ProductTypeId", "Segment");
            return View();
        }

        // POST: Products/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(ProductsView view)
        {
            if (ModelState.IsValid)
            {
                var pic = string.Empty;
                var folder = "~/Content/Images";

                if (view.ImageFile != null)
                {
                    pic = FilesHelper.UploadPhoto(view.ImageFile, folder);
                    pic = string.Format("{0}/{1}", folder, pic);
                }

                var product = ToProduct(view);

                product.Image = pic;
                try
                {
                    db.Products.Add(product);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                catch
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }

            }

            ViewBag.ProductTypeId = new SelectList(CombosHelper.GetProductTypes(), "ProductTypeId", "Segment", view.ProductTypeId);
            return View(view);
        }

        private Product ToProduct(ProductsView view)
        {
            return new Product
            {
                ProductId = view.ProductId,
                ProductTypeId = view.ProductTypeId,
                Name = view.Name,
                ValidityDays = view.ValidityDays,
                Price = view.Price,
                Image = view.Image
            };
        }

        // GET: Products/Edit/5
        [Authorize(Roles = "Products Edit")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Product product = db.Products.Find(id);

            if (product == null)
            {
                return HttpNotFound();
            }
            ViewBag.ProductTypeId = new SelectList(CombosHelper.GetProductTypes(), "ProductTypeId", "Segment", product.ProductTypeId);

            var view = ToView(product);

            return View(view);
        }

        private ProductsView ToView(Product view)
        {
            return new ProductsView
            {
                ProductId = view.ProductId,
                ProductTypeId = view.ProductTypeId,
                Name = view.Name,
                ValidityDays = view.ValidityDays,
                Price = view.Price,
                Image = view.Image
            };
        }

        // POST: Products/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(ProductsView view)
        {
            //se o produto estiver atribuido a uma maquina tem que atualizar na productmachine a data de validade


            //se o produto existe numa maquina
            //verifica se a data e alterada
            bool isValidDate = db.Products.Any(x => x.ProductId == view.ProductId && x.ValidityDays == view.ValidityDays);

            List <ProductsMachine> productsMachineList = db.ProductsMachines.Where(x => x.ProductId == view.ProductId).ToList();
            
            //se existir numa maquina e a data for diferente
            if (productsMachineList.Count > 0 && !isValidDate)
            {
                    //Preencher validade
                int validity = view.ValidityDays;

                    //por cada produto atualiza a data de validade
                foreach (var item in productsMachineList)
                {
                    item.ValidityDate = DateTime.Now.AddDays(validity);
                }
            }
            

            if (ModelState.IsValid)
            {
                try
                {
                    var pic = string.Empty;
                    var folder = "~/Content/Images";

                    if (view.ImageFile != null)
                    {
                        pic = FilesHelper.UploadPhoto(view.ImageFile, folder);
                        pic = string.Format("{0}/{1}", folder, pic);
                    }
                   

                    var product = ToProduct(view);

                    product.Image = pic;

                    db.Entry(product).State = EntityState.Modified;
                    db.SaveChanges();

                    //se existir numa maquina atualiza a datavalidade
                    if (productsMachineList.Count > 0 && !isValidDate)
                    {

                        foreach (var item in productsMachineList)
                        {
                            db.Entry(item).State = EntityState.Modified;
                            db.SaveChanges();
                        }

                    }

                    return RedirectToAction("Index");
                }
                catch
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }

            }
            ViewBag.ProductTypeId = new SelectList(CombosHelper.GetProductTypes(), "ProductTypeId", "Segment", view.ProductTypeId);

            return View(view);
        }

        // GET: Products/Delete/5
        [Authorize(Roles = "Products Delete")]
        public ActionResult Delete(int? id)
        {
            //se o produto existir numa maquina
            var productInMachine = db.ProductsMachines.FirstOrDefault(x => x.ProductId == id);

            if (productInMachine != null)
            {
                ViewBag.Error = "Can not delete because there are products assigned.";

                var productsList = LoadDropDownListAndReturnProducts(0, null);

                return View("Index", productsList);
            }

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Product product = db.Products.Find(id);

            if (product == null)
            {
                return HttpNotFound();
            }

            return View(product);
        }

        // POST: Products/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Product product = db.Products.Find(id);

            if (product == null)
            {
                return HttpNotFound();
            }
            try
            {
                db.Products.Remove(product);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            catch
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            
        }

        /// <summary>
        /// Carrega a DropdownList searchProductType.
        /// Retrona uma lista de products
        /// </summary>
        /// <param name="searchMachine"></param>
        /// <param name="searchCapacity"></param>
        /// <returns></returns>
        private List<Product> LoadDropDownListAndReturnProducts(int? searchProductType, string searchName)
        {
            ViewBag.searchProductType = new SelectList(Helpers.CombosHelper.GetProductTypes(), "ProductTypeId", "Segment", searchProductType);

            var products = db.Products.Include(p => p.ProductType);

            if (!(searchProductType == null || searchProductType == 0))
            {
                products = db.Products.Where(p => p.ProductTypeId == searchProductType).Include(p => p.ProductType);
            }

            if (!string.IsNullOrEmpty(searchName))
            {
                products = db.Products.Where(p => p.Name.ToUpper().Contains(searchName.ToUpper())).Include(p => p.ProductType);
            }

            // Procura por nome e tipo
            if (!string.IsNullOrEmpty(searchName) && !(searchProductType == null || searchProductType == 0))
            {
                products = db.Products.Where(p => p.Name.ToUpper().Contains(searchName.ToUpper()) && p.ProductTypeId == searchProductType).Include(p => p.ProductType);
            }

            return products.ToList();
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
