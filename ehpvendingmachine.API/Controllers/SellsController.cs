﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using ehpvendingmachine.ClassLibrary.Models;

namespace ehpvendingmachine.API.Controllers
{
    public class SellsController : ApiController
    {
        private DataContext db = new DataContext();

        // POST: api/Sells
        [ResponseType(typeof(Sell))]
        public IHttpActionResult PostSell(Sell sell)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Sells.Add(sell);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = sell.SellId }, sell);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}