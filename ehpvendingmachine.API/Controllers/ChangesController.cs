﻿namespace ehpvendingmachine.API.Controllers
{
    using ClassLibrary.Models;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    using System.Linq;
    using System.Net;
    using System.Threading.Tasks;
    using System.Web.Http;
    using System.Web.Http.Description;

    /// <summary>
    /// Contorlador Changes
    /// </summary>
    public class ChangesController : ApiController
    {
        private DataContext db = new DataContext();

        /// <summary>
        /// Atualiza os dados no lado do servidor caso no servidos o campo Altered seja igual a false
        /// </summary>
        /// <param name="id"></param>
        /// <param name="change"></param>
        /// <returns></returns>
        // PUT: api/Changes/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutChange(int id, Change change)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != change.ChangeId)
            {
                return BadRequest();
            }

            // Verificar se Change foi alterado no lado do servidor e se foi não deixa atualizar
            Change changeDb = db.Changes.AsNoTracking().FirstOrDefault(c => c.ChangeId == id);

            // Verificar se registo existe na BD
            if (changeDb == null)
            {
                return StatusCode(HttpStatusCode.NotFound);
            }

            // Verificar se registo foi alterado do lado do servidor
            if (changeDb.Altered)
            {
                return StatusCode(HttpStatusCode.NotModified);
            }

            // Garantir que o campo Altered passa false para o servidor
            change.Altered = false;
            db.Entry(change).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ChangeExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ChangeExists(int id)
        {
            return db.Changes.Count(e => e.ChangeId == id) > 0;
        }
    }
}