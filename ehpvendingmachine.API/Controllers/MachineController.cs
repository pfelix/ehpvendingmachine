﻿namespace ehpvendingmachine.API.Controllers
{
    using Models;
    using ClassLibrary.Models;
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;
    using System.Net.Http;
    using System.Web.Http;
    using System.Threading.Tasks;
    using System.Data.Entity;
    using System;

    /// <summary>
    /// Controlador Machine
    /// </summary>
    public class MachineController : ApiController
    {
        private DataContext db = new DataContext();

        private API.Models.ApplicationDbContext dba = new API.Models.ApplicationDbContext();

        private ArrayList lista = new ArrayList();

        // GET: api/Machine
        /// <summary>
        /// Mostar uma lista das máquinas que o utilizador tem associado
        /// com informação dos produtos e trocos que a máquina tem.
        /// </summary>
        /// <param name="email"></param>
        /// <returns>Retorna a lista das máquinas</returns>
        // GET: api/Machine?userId=7f0b9f08-0f58-4443-bd3d-c408c1ff3d1d
        public async Task<IHttpActionResult> GetMachine(string email)
        {
            var userManager = new UserManager<API.Models.ApplicationUser>(new UserStore<API.Models.ApplicationUser>(dba));

            var user = userManager.FindByEmail(email);

            if (user == null)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.NotFound,"User Not Found"));
            }

            // Lista de máquinas associadas ao user
            List<AssignMachine> assignedMachines = db.AssignMachines.Where(a => a.UserId == user.Id).ToList();

            if (assignedMachines.Count == 0)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.NotFound, "User without associated machines"));
            }

            // Carregar informação por máquina
            for (int i = 0; i < assignedMachines.Count; i++)
            {
                MachineApi machipeApiList = new MachineApi();

                // Id Máquina
                int machineId = assignedMachines[i].MachineId;

                machipeApiList.MachineId = machineId;

                // Lista de produtos associados à(s) máquina(s)
                List<ProductsMachine> productsMachinesList = db.ProductsMachines.Where(m => m.MachineId == machineId).ToList();

                List<ProductsMachinesApi> productsMachinesApiList = new List<ProductsMachinesApi>();

                foreach (var productsMachine in productsMachinesList)
                {
                    var product = db.Products.FirstOrDefault(p => p.ProductId == productsMachine.ProductId);

                    productsMachinesApiList.Add(new ProductsMachinesApi
                    {
                        ProductsMachineId = productsMachine.ProductsMachineId,
                        MachineId = productsMachine.MachineId,
                        ProductId = productsMachine.ProductId,
                        ProductName = product.Name,
                        Price = product.Price,
                        ValidityDate = productsMachine.ValidityDate,
                        ProductQuantity = productsMachine.ProductQuantity,
                        Image = product.Image
                    });
                }

                machipeApiList.ProductsMachines = productsMachinesApiList;

                // Lista de trocos 
                List<Change> changesList = db.Changes.Where(m => m.MachineId == machineId).ToList();

                List<ChangeApi> changeApiList = new List<ChangeApi>();

                foreach (var change in changesList)
                {
                    changeApiList.Add(new ChangeApi
                    {
                        ChangeId = change.ChangeId,
                        CurrencyValueId = change.CurrencyValueId,
                        CurrencyValue = db.CurrencyValues.FirstOrDefault(c => c.CurrencyValueId == change.CurrencyValueId).Currency,
                        MachineId = change.MachineId,
                        Quantity = change.Quantity,
                        QuantityLimit = change.QuantityLimit
                    });
                }

                machipeApiList.Changes = changeApiList;

                
                // Lista de historico por resolver
                List<HistoricAlert> historicAlertList =
                    db.HistoricAlerts.Where(m => m.MachineId == machineId && m.ResolutionDate == null).ToList();

                List<HistoricAlertApi> historicAlertApiList = new List<HistoricAlertApi>();

                foreach (var historicAlert in historicAlertList)
                {
                    historicAlertApiList.Add(new HistoricAlertApi
                    {
                        HistoricAlertId = historicAlert.HistoricAlertId,
                        MachineId = historicAlert.MachineId,
                        SetAlertId = historicAlert.SetAlertId,
                        Date = historicAlert.Date
                    });
                }

                machipeApiList.HistoricAlerts = historicAlertApiList;


                // Adicionar à lista para enviar
                lista.Add(machipeApiList);

                // Passar a falso campo alterado do change
                foreach (var change in changesList)
                {
                    change.Altered = false;
                    db.Entry(change).State = EntityState.Modified;
                }

                // Passar a falso campo alterado do ProductsMachine
                foreach (var productsMachines in productsMachinesList)
                {
                    productsMachines.Altered = false;
                    db.Entry(productsMachines).State = EntityState.Modified;
                }

                try
                {
                    await db.SaveChangesAsync();
                }
                catch
                {
                    return ResponseMessage(Request.CreateResponse(HttpStatusCode.NotModified));
                }


            }

            return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, lista));
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}