﻿namespace ehpvendingmachine.API.Controllers
{
    using ClassLibrary.Models;
    using System.Threading.Tasks;
    using System.Web.Http;
    using System.Web.Http.Description;

    /// <summary>
    /// Controlador Historic Alerts
    /// </summary>
    public class HistoricAlertsController : ApiController
    {
        private DataContext db = new DataContext();

        // POST: api/HistoricAlerts
        /// <summary>
        /// Insere no servidor o historico alert
        /// </summary>
        /// <param name="historicAlert"></param>
        /// <returns></returns>
        [ResponseType(typeof(HistoricAlert))]
        public async Task<IHttpActionResult> PostHistoricAlert(HistoricAlert historicAlert)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.HistoricAlerts.Add(historicAlert);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = historicAlert.HistoricAlertId }, historicAlert);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}