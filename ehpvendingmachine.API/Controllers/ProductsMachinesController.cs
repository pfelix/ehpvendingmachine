﻿namespace ehpvendingmachine.API.Controllers
{
    using ClassLibrary.Models;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    using System.Linq;
    using System.Net;
    using System.Threading.Tasks;
    using System.Web.Http;
    using System.Web.Http.Description;

    /// <summary>
    /// Controlador Products Machine
    /// </summary>
    public class ProductsMachinesController : ApiController
    {
        private DataContext db = new DataContext();

        /// <summary>
        /// Atualiza os dados no lado do servidor caso no servidos o campo Altered seja igual a false
        /// </summary>
        /// <param name="id"></param>
        /// <param name="productsMachine"></param>
        /// <returns></returns>
        // PUT: api/ProductsMachines/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutProductsMachine(int id, ProductsMachine productsMachine)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != productsMachine.ProductsMachineId)
            {
                return BadRequest();
            }

            // Verificar se Change foi alterado no lado do servidor e se foi não deixa atualizar
            ProductsMachine productsMachineDb = db.ProductsMachines.AsNoTracking().FirstOrDefault(c => c.ProductsMachineId == id);

            // Caso o produto que esteia a ser inserido não exista na BD
            if (productsMachineDb == null)
            {
                return StatusCode(HttpStatusCode.NotFound);
            }

            // Verificar se o produto foi alterado do lado do servidor
            if (productsMachineDb.Altered)
            {
                return StatusCode(HttpStatusCode.NotModified);
            }

            // Garantir que o campo Altered passa false para o servidor
            productsMachine.Altered = false;
            productsMachine.ValidityDate = productsMachineDb.ValidityDate;

            db.Entry(productsMachine).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ProductsMachineExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ProductsMachineExists(int id)
        {
            return db.ProductsMachines.Count(e => e.ProductsMachineId == id) > 0;
        }
    }
}