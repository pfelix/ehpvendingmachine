﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ehpvendingmachine.API.Models
{
    public class ChangeApi
    {
        public int ChangeId { get; set; }

        public int CurrencyValueId { get; set; }

        public double CurrencyValue { get; set; }

        public int MachineId { get; set; }

        public int Quantity { get; set; }

        public int QuantityLimit { get; set; }

    }
}