﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ehpvendingmachine.ClassLibrary.Models;

namespace ehpvendingmachine.API.Models
{
    public class MachineApi
    {
        public int MachineId { get; set; }

        public List<ProductsMachinesApi> ProductsMachines { get; set; }

        public List<ChangeApi> Changes { get; set; }

        public List<HistoricAlertApi> HistoricAlerts { get; set; }

    }
}