﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ehpvendingmachine.API.Models
{
    public class HistoricAlertApi
    {
        public int HistoricAlertId { get; set; }

        public int MachineId { get; set; }

        public int SetAlertId { get; set; }

        public DateTime Date { get; set; }

    }
}