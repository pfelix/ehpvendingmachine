﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ehpvendingmachine.API.Models
{
    public class ProductsMachinesApi
    {
        public int ProductsMachineId { get; set; }

        public int MachineId { get; set; }

        public int ProductId { get; set; }

        public string ProductName { get; set; }

        public DateTime ValidityDate { get; set; }

        public string Image { get; set; }

        public int ProductQuantity { get; set; }

        public double Price { get; set; }
    }
}